namespace Bridge.Academies.Api.ComponentTest.Factories
{
    using System;
    using System.Collections.Generic;
    using Bridge.Academies.Api.Models;

    public static class AcademyFactory
    {
        private static Random random = new Random();

        public static Academy Get()
        {
            var academyId = random.Next();
            return new Academy()
            {
                Active = true,
                AcademyID = academyId,
                AcademyLocation = new AcademyLocation()
                {
                    AcademyID = academyId,
                    GPSLat = 51.501364,
                    GPSLong = -0.14189
                },
                AcademyName = "Academy Name",
                LaunchDate = new DateTime(2017, 1, 1),
                Region = new Region()
            };
        }

        public static Academy WithAcademyId(this Academy academy, int academyId)
        {
            academy.AcademyID = academyId;
            return academy;
        }

        public static Academy WithStartDate(this Academy academy, DateTime date)
        {
            academy.LaunchDate = date.Date;
            return academy;
        }

        public static Academy WithClassrooms(this Academy academy, List<Classroom> classrooms)
        {
            academy.Classrooms = classrooms;
            return academy;
        }
    }
}
