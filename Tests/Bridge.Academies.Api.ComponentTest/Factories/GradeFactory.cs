namespace Bridge.Academies.Api.ComponentTest.Factories
{
    using System;
    using Bridge.Academies.Api.Models;

    public static class GradeFactory
    {
        private static Random random = new Random();

        public static Grades Get() =>
            new Grades()
            {
                Active = true,
                GradeAbbreviation = "ST1",
                GradeId = random.Next(),
                GradeName = "Baby Class",
                MaxAgeOffset = null,
                MinAgeOffset = null,
                SortOrder = 1
            };
    }
}
