namespace Bridge.Academies.Api.ComponentTest.Factories
{
    using System;
    using Bridge.Academies.Api.Models;

    public static class ClassroomFactory
    {
        private static Random random = new Random();

        public static Classroom Get() =>
            new Classroom()
            {
                Active = true,
                AcademyId = 1,
                ClassroomName = "Name",
                ClassroomId = random.Next(),
                GradeId = 1
            };

        public static Classroom WithAcademyId(this Classroom classrooms, int academyId)
        {
            classrooms.AcademyId = academyId;
            return classrooms;
        }

        public static Classroom WithStartDate(this Classroom classrooms, DateTime date)
        {
            classrooms.EffectiveDate = date.Date;
            return classrooms;
        }

        public static Classroom WithEndDate(this Classroom classrooms, DateTime date)
        {
            classrooms.DateInactivated = date.Date;
            return classrooms;
        }
    }
}
