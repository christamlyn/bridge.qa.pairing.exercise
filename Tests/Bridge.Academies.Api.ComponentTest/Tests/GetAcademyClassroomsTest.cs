namespace Bridge.Academies.Api.ComponentTest.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.ComponentTest.Factories;
    using Bridge.Academies.Api.ComponentTest.Fixtures;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.ViewModels;
    using Shouldly;
    using Xunit;

    public class GetAcademyClassroomsTest : IClassFixture<TestFixture<InMemoryTestStartup>>
    {
        private readonly IBridgeDbContext bridgeDbContext;
        private readonly HttpClient client;

        public GetAcademyClassroomsTest(TestFixture<InMemoryTestStartup> fixture)
        {
            this.bridgeDbContext = fixture.BridgeDbContext;
            this.client = fixture.Client;
            this.bridgeDbContext.Reset().Wait();
        }

        [Fact]
        public async Task Get_Classroom_For_Academy_On_DateAsync()
        {
            var academyId = 1;
            var tenantId = 1;
            var activeOn = new DateTime(2018, 1, 1);
            var requestParameters = new AcademyClassroomRequest()
            {
                ActiveOn = activeOn.Date,
                TenantId = tenantId
            };

            var classroom = ClassroomFactory
                .Get()
                .WithAcademyId(academyId)
                .WithStartDate(activeOn.AddDays(-1))
                .WithEndDate(activeOn.AddDays(1));

            var academy = AcademyFactory
                .Get()
                .WithAcademyId(academyId)
                .WithStartDate(activeOn.AddDays(-1))
                .WithClassrooms(new List<Models.Classroom> { classroom });
            this.bridgeDbContext.Academies.Add(academy);
            await this.bridgeDbContext.SaveChangesAsync();

            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri($"/classrooms/{academyId}?&activeOn={requestParameters.ActiveOn:yyyy-MM-dd}&tenantId={requestParameters.TenantId}", UriKind.Relative),
            };

            var response = await this.client.SendAsync(request);
            Console.WriteLine($"Content: {await response.Content.ReadAsStringAsync()}");
            response.StatusCode.ShouldBe(HttpStatusCode.OK);
            var classrooms = await response.Content.ReadAsAsync<List<ViewModels.Classroom>>();
            classrooms.Count.ShouldBe(1);
        }
    }
}