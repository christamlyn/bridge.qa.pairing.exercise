namespace Bridge.Academies.Api.ComponentTest.Fixtures
{
    using System;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class InMemoryTestStartup : TestStartup
    {
        private readonly string bridgeDatabaseName = Guid.NewGuid().ToString();
        private readonly string warehouseDatabaseName = Guid.NewGuid().ToString();

        public InMemoryTestStartup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
            : base(configuration, hostingEnvironment)
        {
            InMemoryBridgeDbContext.DatabaseName = this.bridgeDatabaseName;
            InMemoryWarehouseDbContext.DatabaseName = this.warehouseDatabaseName;
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            services.AddScoped<IBridgeDbContext, BridgeDbContext>(x => new InMemoryBridgeDbContext());
            services.AddScoped<IWarehouseDbContext, WarehouseDbContext>(x => new InMemoryWarehouseDbContext());
        }
    }
}