namespace Bridge.Academies.Api.ComponentTest.Fixtures
{
    using System;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.ChangeTracking;

    public static class BridgeDbContextExtensions
    {
        public static Task Reset(this IBridgeDbContext applicationDbContext)
        {
            Clear(applicationDbContext.Academies, applicationDbContext.Entry);
            Clear(applicationDbContext.AcademyLocations, applicationDbContext.Entry);
            Clear(applicationDbContext.Classrooms, applicationDbContext.Entry);
            Clear(applicationDbContext.Grades, applicationDbContext.Entry);
            Clear(applicationDbContext.Regions, applicationDbContext.Entry);
            Clear(applicationDbContext.Users, applicationDbContext.Entry);

            return applicationDbContext.SaveChangesAsync();
        }

        private static void Clear<T>(DbSet<T> dbSet, Func<T, EntityEntry<T>> getEntry)
            where T : class
        {
            foreach (var item in dbSet)
            {
                var entry = getEntry(item);
                if (entry.State != EntityState.Detached)
                {
                    entry.State = EntityState.Deleted;
                }
            }
        }
    }
}
