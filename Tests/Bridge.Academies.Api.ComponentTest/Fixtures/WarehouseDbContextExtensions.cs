namespace Bridge.Academies.Api.ComponentTest.Fixtures
{
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories.EntityFramework;

    public static class WarehouseDbContextExtensions
    {
        public static Task Reset(this IWarehouseDbContext warehouseDbContext)
        {
            warehouseDbContext.AcademicTerms.RemoveRange(warehouseDbContext.AcademicTerms);
            warehouseDbContext.Academies.RemoveRange(warehouseDbContext.Academies);
            warehouseDbContext.Dates.RemoveRange(warehouseDbContext.Dates);
            warehouseDbContext.GradeDailyMetrics.RemoveRange(warehouseDbContext.GradeDailyMetrics);
            warehouseDbContext.PupilDailyMetrics.RemoveRange(warehouseDbContext.PupilDailyMetrics);
            warehouseDbContext.Pupils.RemoveRange(warehouseDbContext.Pupils);
            return warehouseDbContext.SaveChangesAsync();
        }
    }
}
