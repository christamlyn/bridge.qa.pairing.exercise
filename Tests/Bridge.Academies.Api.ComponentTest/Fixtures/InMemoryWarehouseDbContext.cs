namespace Bridge.Academies.Api.ComponentTest.Fixtures
{
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Microsoft.EntityFrameworkCore;
    using Moq;

    public class InMemoryWarehouseDbContext : WarehouseDbContext
    {
        public InMemoryWarehouseDbContext()
            : base(new Mock<ITenantService>().Object)
        {
        }

        public static string DatabaseName { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseInMemoryDatabase(DatabaseName);
    }
}
