namespace Bridge.Academies.Api.ComponentTest.Fixtures
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Settings;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.TestHost;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;

    public class TestFixture<T> : IDisposable
       where T : class
    {
        private readonly TestServer server;
        private readonly IServiceScope serviceScope;

        public TestFixture()
        {
            var builder = new WebHostBuilder()
                .ConfigureAppConfiguration((hostingContext, config) =>
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true))
                .UseDefaultServiceProvider((context, options) => options.ValidateScopes = true)
                .UseStartup<T>();

            this.server = new TestServer(builder);

            this.serviceScope = this.server.Host.Services.GetService<IServiceScopeFactory>().CreateScope();
            var serviceProvider = this.serviceScope.ServiceProvider;
            this.BridgeDbContext = serviceProvider.GetRequiredService<IBridgeDbContext>();
            this.WarehouseDbContext = serviceProvider.GetRequiredService<IWarehouseDbContext>();
            this.AppSettings = serviceProvider.GetRequiredService<IOptions<AppSettings>>().Value;

            this.Client = this.server.CreateClient();
            this.Client.BaseAddress = new Uri("http://localhost");
            this.Client.DefaultRequestHeaders.Add("X-Request-ID", Guid.NewGuid().ToString());
            this.Client.DefaultRequestHeaders.Add("User-Agent", "Academy-Manager/19.4.0 (Android 4.4.2)");
            var credentials = this.AppSettings.BasicAuthentication.Credentials.First();
            this.Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
               "Basic",
               Convert.ToBase64String(Encoding.ASCII.GetBytes($"{credentials.Username}:{credentials.Password}")));
            this.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            this.Client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
        }

        public IBridgeDbContext BridgeDbContext { get; }

        public IWarehouseDbContext WarehouseDbContext { get; }

        public AppSettings AppSettings { get; }

        public HttpClient Client { get; }

        public void Dispose()
        {
            this.serviceScope.Dispose();
            this.Client.Dispose();
            this.server.Dispose();
        }
    }
}