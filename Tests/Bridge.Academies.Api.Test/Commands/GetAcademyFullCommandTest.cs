namespace Bridge.Academies.Api.Test.Commands
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class GetAcademyFullCommandTest : IDisposable
    {
        private readonly Mock<IAcademiesRepository> academiesRepositoryMock;
        private readonly Mock<ITranslator<Models.Academy, AcademyFull>> academyTranslatorMock;
        private readonly GetAcademyFullCommand command;
        private readonly Mock<ITenantService> tenantService;
        private int? tenantId = 1;

        public GetAcademyFullCommandTest()
        {
            this.academiesRepositoryMock = new Mock<IAcademiesRepository>(MockBehavior.Strict);
            this.academyTranslatorMock = new Mock<ITranslator<Models.Academy, AcademyFull>>(MockBehavior.Strict);
            this.tenantService = new Mock<ITenantService>(MockBehavior.Strict);
            this.tenantService.Setup(x => x.ValidateTenant(this.tenantId)).Returns(new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary());

            this.command = new GetAcademyFullCommand(
                this.academiesRepositoryMock.Object,
                this.academyTranslatorMock.Object,
                this.tenantService.Object);
        }

        [Fact]
        public async Task ExecuteAsync_AcademyFound_ReturnsOk()
        {
            var model = new Models.Academy();
            var cancellationTokenSource = new CancellationTokenSource();
            this.academiesRepositoryMock
                .Setup(x => x.GetAcademy(1, true, cancellationTokenSource.Token))
                .Returns(Task.FromResult(model));
            this.academyTranslatorMock.Setup(x => x.Translate(model, It.IsAny<AcademyFull>()));

            var result = await this.command.ExecuteAsync(this.tenantId, 1, cancellationTokenSource.Token);

            Assert.IsType<OkObjectResult>(result);
            var okResult = (OkObjectResult)result;
            Assert.NotNull(okResult.Value);
            Assert.IsType<AcademyFull>(okResult.Value);
        }

        [Fact]
        public async Task ExecuteAsync_AcademyNotFound_ReturnsOk()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            this.academiesRepositoryMock
                .Setup(x => x.GetAcademy(1, true, cancellationTokenSource.Token))
                .Returns(Task.FromResult<Models.Academy>(null));

            var result = await this.command.ExecuteAsync(this.tenantId, 1, cancellationTokenSource.Token);

            Assert.IsType<NotFoundResult>(result);
        }

        public void Dispose() =>
            Mock.VerifyAll(
                this.academiesRepositoryMock,
                this.academyTranslatorMock,
                this.tenantService);
    }
}
