namespace Bridge.Academies.Api.Test.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class GetAllAcademiesCommandTest : IDisposable
    {
        private readonly Mock<IAcademiesRepository> academiesRepositoryMock;
        private readonly Mock<ITranslator<Models.Academy, Academy>> academyTranslatorMock;
        private readonly Mock<ITenantService> tenantService;
        private readonly GetAllAcademiesCommand command;
        private readonly int? tenantId = 1;

        public GetAllAcademiesCommandTest()
        {
            this.academiesRepositoryMock = new Mock<IAcademiesRepository>(MockBehavior.Strict);
            this.academyTranslatorMock = new Mock<ITranslator<Models.Academy, Academy>>(MockBehavior.Strict);
            this.tenantService = new Mock<ITenantService>(MockBehavior.Strict);

            this.command = new GetAllAcademiesCommand(
                this.academiesRepositoryMock.Object,
                this.academyTranslatorMock.Object,
                this.tenantService.Object);
        }

        [Fact]
        public async Task ExecuteAsync_AcademyFound_ReturnsOk()
        {
            var models = new List<Models.Academy>()
            {
                new Models.Academy()
            };
            var academyIds = new List<int>() { 1, 2 };
            var request = new AcademiesRequest()
            {
                AcademyId = academyIds,
                IsActive = true,
                ModifiedSince = null,
                TenantId = this.tenantId
            };
            var cancellationTokenSource = new CancellationTokenSource();
            this.academiesRepositoryMock
                .Setup(x => x.GetAllAcademies(academyIds, true, false, null, cancellationTokenSource.Token))
                .ReturnsAsync(models);
            this.academyTranslatorMock.Setup(x => x.Translate(models.First(), It.IsAny<Academy>()));
            this.tenantService.Setup(x => x.ValidateTenant(this.tenantId)).Returns(new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary());

            var result = await this.command.ExecuteAsync(request, cancellationTokenSource.Token);

            Assert.IsType<OkObjectResult>(result);
            var okResult = (OkObjectResult)result;
            Assert.NotNull(okResult.Value);
            Assert.IsType<List<Academy>>(okResult.Value);
        }

        [Fact]
        public async Task ExecuteAsync_TenantNotFound_ReturnsBadRequest()
        {
            var invalidTenantId = 2;
            var models = new List<Models.Academy>()
            {
                new Models.Academy()
            };
            var academyIds = new List<int>() { 1, 2 };
            var request = new AcademiesRequest()
            {
                AcademyId = academyIds,
                IsActive = true,
                ModifiedSince = null,
                TenantId = invalidTenantId
            };
            var cancellationTokenSource = new CancellationTokenSource();
            var invalidModel = new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary();
            invalidModel.AddModelError("key", "error");
            this.tenantService.Setup(x => x.ValidateTenant(invalidTenantId)).Returns(invalidModel);

            var result = await this.command.ExecuteAsync(request, cancellationTokenSource.Token);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        public void Dispose() =>
            Mock.VerifyAll(
                this.academiesRepositoryMock,
                this.academyTranslatorMock,
                this.tenantService);
    }
}
