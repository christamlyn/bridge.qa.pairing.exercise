namespace Bridge.Academies.Api.Test.Commands
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class GetAcademyCommandTest : IDisposable
    {
        private readonly Mock<IAcademiesRepository> academiesRepositoryMock;
        private readonly Mock<ITranslator<Models.Academy, Academy>> academyTranslatorMock;
        private readonly Mock<ITenantService> tenantService;
        private readonly GetAcademyCommand command;
        private readonly int? tenantId = 1;

        public GetAcademyCommandTest()
        {
            this.academiesRepositoryMock = new Mock<IAcademiesRepository>(MockBehavior.Strict);
            this.academyTranslatorMock = new Mock<ITranslator<Models.Academy, Academy>>(MockBehavior.Strict);
            this.tenantService = new Mock<ITenantService>(MockBehavior.Strict);

            this.command = new GetAcademyCommand(
                this.academiesRepositoryMock.Object,
                this.academyTranslatorMock.Object,
                this.tenantService.Object);
        }

        [Fact]
        public async Task ExecuteAsync_AcademyFound_ReturnsOk()
        {
            var model = new Models.Academy();
            var cancellationTokenSource = new CancellationTokenSource();
            this.academiesRepositoryMock
                .Setup(x => x.GetAcademy(1, false, cancellationTokenSource.Token))
                .Returns(Task.FromResult(model));
            this.academyTranslatorMock.Setup(x => x.Translate(model, It.IsAny<Academy>()));
            this.tenantService.Setup(x => x.ValidateTenant(this.tenantId)).Returns(new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary());

            var result = await this.command.ExecuteAsync(this.tenantId, 1, cancellationTokenSource.Token);

            Assert.IsType<OkObjectResult>(result);
            var okResult = (OkObjectResult)result;
            Assert.NotNull(okResult.Value);
            Assert.IsType<Academy>(okResult.Value);
        }

        [Fact]
        public async Task ExecuteAsync_TenantNotFound_ReturnsBadRequest()
        {
            var model = new Models.Academy();
            var modelState = new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary();
            modelState.AddModelError("key", "value");
            var cancellationTokenSource = new CancellationTokenSource();
            var invalidTenant = 2;
            this.tenantService.Setup(x => x.ValidateTenant(invalidTenant)).Returns(modelState);

            var result = await this.command.ExecuteAsync(invalidTenant, 1, cancellationTokenSource.Token);

            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task ExecuteAsync_AcademyNotFound_ReturnsOk()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            this.academiesRepositoryMock
                .Setup(x => x.GetAcademy(1, false, cancellationTokenSource.Token))
                .Returns(Task.FromResult<Models.Academy>(null));
            this.tenantService.Setup(x => x.ValidateTenant(this.tenantId)).Returns(new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary());

            var result = await this.command.ExecuteAsync(this.tenantId, 1, cancellationTokenSource.Token);

            Assert.IsType<NotFoundResult>(result);
        }

        public void Dispose() =>
            Mock.VerifyAll(
                this.academiesRepositoryMock,
                this.academyTranslatorMock,
                this.tenantService);
    }
}
