namespace Bridge.Academies.Api.Test.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class GetAcademiesFullCommandTest : IDisposable
    {
        private readonly Mock<IAcademiesRepository> academiesRepositoryMock;
        private readonly Mock<ITranslator<Models.Academy, AcademyAll>> academyTranslatorMock;
        private readonly GetAcademiesFullCommand command;
        private readonly Mock<ITenantService> tenantService;
        private int? tenantId = 1;

        public GetAcademiesFullCommandTest()
        {
            this.academiesRepositoryMock = new Mock<IAcademiesRepository>(MockBehavior.Strict);
            this.academyTranslatorMock = new Mock<ITranslator<Models.Academy, AcademyAll>>(MockBehavior.Strict);
            this.tenantService = new Mock<ITenantService>(MockBehavior.Strict);
            this.tenantService.Setup(x => x.ValidateTenant(this.tenantId))
                .Returns(new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary());

            this.command = new GetAcademiesFullCommand(
                this.academiesRepositoryMock.Object,
                this.academyTranslatorMock.Object,
                this.tenantService.Object);
        }

        [Fact]
        public async Task ExecuteAsync_AcademyFound_ReturnsOk()
        {
            var models = new List<Models.Academy>()
            {
                new Models.Academy()
            };
            var academyIds = new List<int>() { 1, 2 };
            var request = new AcademiesRequest()
            {
                AcademyId = academyIds,
                IsActive = true,
                ModifiedSince = null,
                TenantId = this.tenantId
            };
            var cancellationTokenSource = new CancellationTokenSource();

            this.academiesRepositoryMock
                 .Setup(x => x.GetAllAcademies(
                     request.AcademyId,
                     request.IsActive,
                     true,
                     request.ModifiedSince,
                     cancellationTokenSource.Token))
                     .ReturnsAsync(models);

            this.academyTranslatorMock
                .Setup(x => x.Translate(models.First(), It.IsAny<AcademyAll>()));

            this.tenantService
                .Setup(x => x.ValidateTenant(this.tenantId))
                .Returns(new Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary());

            var result = await this.command.ExecuteAsync(request, cancellationTokenSource.Token);

            Assert.IsType<OkObjectResult>(result);
            var okResult = (OkObjectResult)result;
            Assert.NotNull(okResult.Value);
            Assert.IsType<List<AcademyAll>>(okResult.Value);
        }

        public void Dispose() =>
            Mock.VerifyAll(
                this.academiesRepositoryMock,
                this.academyTranslatorMock,
                this.tenantService);
    }
}
