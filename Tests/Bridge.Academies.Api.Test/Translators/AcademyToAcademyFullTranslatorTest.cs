﻿namespace Bridge.Academies.Api.Test.Translators
{
    using System;
    using Bridge.Academies.Api.Translators;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class AcademyToAcademyFullTranslatorTest
    {
        private Mock<ITranslator<Models.AcademyLocation, Coordinates>> academyLocationToCoordinatesTranslatorMock;
        private Mock<ITranslator<Models.Region, Location>> regionToLocationTranslatorMock;

        [Fact]
        public void Translate_Default_Successful()
        {
            var model = new Models.Academy()
            {
                AcademyID = 1,
                Active = true,
                AcademyCode = "Code",
                AcademyLocation = new Models.AcademyLocation(),
                AcademyName = "Name",
                LaunchDate = DateTime.UtcNow,
                Region = new Models.Region(),
                ShortCode = "ShortCode",
                RegionID = 1
            };
            var viewModel = new AcademyFull();
            var translator = this.GetTranslator();
            this.regionToLocationTranslatorMock
                .Setup(x => x.Translate(model.Region, It.IsAny<Location>()));
            this.academyLocationToCoordinatesTranslatorMock
                .Setup(x => x.Translate(model.AcademyLocation, It.IsAny<Coordinates>()));

            translator.Translate(model, viewModel);

            Assert.Equal(viewModel.AcademyId, model.AcademyID);
            Assert.Equal(viewModel.Active, model.Active);
            Assert.Equal(viewModel.Code, model.AcademyCode);
            Assert.Equal(viewModel.LaunchDate, model.LaunchDate);
            Assert.Equal(viewModel.Name, model.AcademyName);
            Assert.Equal(viewModel.ShortCode, model.ShortCode);
            Assert.Equal(viewModel.RegionId, model.RegionID);
            this.VerifyAll();
        }

        [Fact]
        public void Translate_NoLocationOrRegion_TranslatorsNotCalled()
        {
            var model = new Models.Academy()
            {
                AcademyID = 1,
                Active = true,
                AcademyCode = "Code",
                AcademyName = "Name",
                LaunchDate = DateTime.UtcNow,
                ShortCode = "ShortCode"
            };
            var viewModel = new AcademyFull();
            var translator = this.GetTranslator();

            translator.Translate(model, viewModel);

            Assert.Equal(viewModel.AcademyId, model.AcademyID);
            Assert.Equal(viewModel.Active, model.Active);
            Assert.Equal(viewModel.Code, model.AcademyCode);
            Assert.Equal(viewModel.LaunchDate, model.LaunchDate);
            Assert.Equal(viewModel.Name, model.AcademyName);
            Assert.Equal(viewModel.ShortCode, model.ShortCode);
            this.VerifyAll();
        }

        private AcademyToAcademyFullTranslator GetTranslator()
        {
            this.academyLocationToCoordinatesTranslatorMock = new Mock<ITranslator<Models.AcademyLocation, Coordinates>>(MockBehavior.Strict);
            this.regionToLocationTranslatorMock = new Mock<ITranslator<Models.Region, Location>>(MockBehavior.Strict);

            return new AcademyToAcademyFullTranslator(
                this.academyLocationToCoordinatesTranslatorMock.Object,
                this.regionToLocationTranslatorMock.Object);
        }

        private void VerifyAll()
        {
            Mock.VerifyAll(
                this.academyLocationToCoordinatesTranslatorMock,
                this.regionToLocationTranslatorMock);
        }
    }
}
