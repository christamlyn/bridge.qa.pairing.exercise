namespace Bridge.Academies.Api.Test.Translators
{
    using System;
    using Bridge.Academies.Api.Translators;
    using Bridge.Academies.Api.ViewModels;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class AcademyToAcademyTranslatorTest
    {
        [Fact]
        public void Translate_Default_Successful()
        {
            var source = new Models.Academy()
            {
                AcademyCode = "Code",
                AcademyID = 1,
                AcademyManagerContactPhone = "123",
                AcademyName = "Name",
                Active = true,
                LaunchDate = DateTime.UtcNow,
                RegionID = 1
            };
            var destination = new Academy();
            var translator = new AcademyToAcademyTranslator();

            translator.Translate(source, destination);

            Assert.Equal(source.AcademyID, destination.AcademyId);
            Assert.Equal(source.Active, destination.Active);
            Assert.Equal(source.AcademyCode, destination.Code);
            Assert.Equal(source.LaunchDate, destination.LaunchDate);
            Assert.Equal(source.AcademyName, destination.Name);
            Assert.Equal(source.ShortCode, destination.ShortCode);
            Assert.Equal(source.AcademyManagerContactPhone, destination.Phone);
            Assert.Equal(source.RegionID, destination.RegionId);
        }
    }
}
