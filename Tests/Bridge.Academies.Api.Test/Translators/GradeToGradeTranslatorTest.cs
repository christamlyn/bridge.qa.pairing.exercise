﻿namespace Bridge.Academies.Api.Test.Translators
{
    using Bridge.Academies.Api.Translators;
    using Xunit;

    public class GradeToGradeTranslatorTest
    {
        private GradeToGradeTranslator translator = new GradeToGradeTranslator();

        [Fact]
        public void Translate_Default_Successful()
        {
            var source = new Models.Grades()
            {
                Active = true,
                GradeName = "Nursery",
                GradeAbbreviation = "NU",
                MaxPupils = 50,
                HasPlacement = true,
                MaxAgeOffset = 10,
                MinAgeOffset = 7,
                MinPlacementScore = 0,
                MaxPlacementScore = 10,
                SortOrder = 1
            };
            var destination = new ViewModels.Grade();

            this.translator.Translate(source, destination);

            Assert.Equal(source.GradeName, destination.Name);
            Assert.Equal(source.GradeAbbreviation, destination.Code);
            Assert.Equal(source.SortOrder, destination.Order);
        }
    }
}
