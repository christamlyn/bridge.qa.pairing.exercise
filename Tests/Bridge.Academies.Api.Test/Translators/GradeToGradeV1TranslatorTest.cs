﻿namespace Bridge.Academies.Api.Test.Translators
{
    using Bridge.Academies.Api.Translators;
    using Xunit;

    public class GradeToGradeV1TranslatorTest
    {
        private readonly GradeToGradeV1Translator translator = new GradeToGradeV1Translator();

        [Fact]
        public void Translate_Default_Successful()
        {
            var source = new Models.Grades()
            {
                Active = true,
                GradeName = "Nursery",
                GradeAbbreviation = "NU",
                MaxPupils = 50,
                HasPlacement = true,
                MaxAgeOffset = 10,
                MinAgeOffset = 7,
                MinPlacementScore = 0,
                MaxPlacementScore = 10,
                SortOrder = 1
            };
            var destination = new ViewModels.GradeV1();

            this.translator.Translate(source, destination);

            Assert.Equal(source.Active, destination.Active);
            Assert.Equal(source.GradeName, destination.GradeName);
            Assert.Equal(source.GradeAbbreviation, destination.GradeAbbreviation);
            Assert.Equal(source.HasPlacement, destination.HasPlacement);
            Assert.Equal(source.MaxPlacementScore, destination.MaxAgeOffset);
            Assert.Equal(source.MaxPlacementScore, destination.MaxPlacementScore);
            Assert.Equal(source.MaxPupils, destination.MaxPupils);
            Assert.Equal(source.MinAgeOffset, destination.MinAgeOffset);
            Assert.Equal(source.MinPlacementScore, destination.MinPlacementScore);
            Assert.Equal(source.SortOrder, destination.SortOrder);
        }
    }
}
