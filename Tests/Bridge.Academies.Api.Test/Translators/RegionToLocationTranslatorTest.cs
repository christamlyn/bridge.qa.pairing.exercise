﻿namespace Bridge.Academies.Api.Test.Translators
{
    using Bridge.Academies.Api.Translators;
    using Bridge.Academies.Api.ViewModels;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class RegionToLocationTranslatorTest
    {
        [Fact]
        public void Translate_Default_Translated()
        {
            var region = new Models.Region()
            {
                Country = "Country",
                LevelOne = "1",
                LevelTwo = "2",
                LevelThree = "3",
                LevelFour = "4",
                LevelFive = "5",
                LevelSix = "6",
                LevelSeven = "7"
            };
            var location = new Location();
            var translator = new RegionToLocationTranslator();

            translator.Translate(region, location);

            Assert.Equal(region.Country, location.Country);
            Assert.Equal(region.LevelOne, location.Level1);
            Assert.Equal(region.LevelTwo, location.Level2);
            Assert.Equal(region.LevelThree, location.Level3);
            Assert.Equal(region.LevelFour, location.Level4);
            Assert.Equal(region.LevelFive, location.Level5);
            Assert.Equal(region.LevelSix, location.Level6);
            Assert.Equal(region.LevelSeven, location.Level7);
        }
    }
}
