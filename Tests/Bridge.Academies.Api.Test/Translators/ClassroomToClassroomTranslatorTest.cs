namespace Bridge.Academies.Api.Test.Translators
{
    using System;
    using Bridge.Academies.Api.Translators;
    using Xunit;

    public class ClassroomToClassroomTranslatorTest
    {
        [Fact]
        public void Translate_Default_Successful()
        {
            var source = new Models.Classroom()
            {
                Active = true,
                ClassroomName = "Test",
                DateInactivated = null,
                EffectiveDate = DateTime.Now
            };
            var destination = new ViewModels.Classroom();
            var translator = new ClassroomToClassroomTranslator();

            translator.Translate(source, destination);

            Assert.Equal(source.Active, destination.Active);
            Assert.Equal(source.ClassroomName, destination.ClassroomName);
            Assert.Equal(source.DateInactivated, destination.DateInactivated);
            Assert.Equal(source.EffectiveDate, destination.EffectiveDate);
        }
    }
}