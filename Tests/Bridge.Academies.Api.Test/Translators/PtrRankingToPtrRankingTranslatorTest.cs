﻿namespace Bridge.Academies.Api.Test.Translators
{
    using System;
    using Bridge.Academies.Api.Translators;
    using Bridge.Academies.Api.ViewModels;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class PtrRankingToPtrRankingTranslatorTest
    {
        [Fact]
        public void Translate_Default_Successful()
        {
            var source = new Models.PtrRanking()
            {
                AcademyId = 1,
                AicPTR = 20,
                Ranking = 10,
                TotalAcademies = 50
            };
            var destination = new PtrRanking();
            var translator = new PtrRankingToPtrRankingTranslator();

            translator.Translate(source, destination);

            Assert.Equal(source.AcademyId, destination.AcademyId);
            Assert.Equal(source.AicPTR, destination.AicPTR);
            Assert.Equal(source.Ranking, destination.Ranking);
            Assert.Equal(source.TotalAcademies, destination.TotalAcademies);
        }
    }
}
