﻿namespace Bridge.Academies.Api.Test.Translators
{
    using Bridge.Academies.Api.Translators;
    using Bridge.Academies.Api.ViewModels;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class AcademyLocationToCoordinatesTranslatorTest
    {
        [Fact]
        public void Translate_Default_Translated()
        {
            Models.AcademyLocation location = new Models.AcademyLocation()
            {
                AcademyID = 1,
                GPSLat = 2,
                GPSLong = 3,
                Radius = 0.22,
            };
            var coordinates = new Coordinates();
            var translator = new AcademyLocationToCoordinatesTranslator();

            translator.Translate(location, coordinates);

            Assert.Equal(location.GPSLat, coordinates.Latitude);
            Assert.Equal(location.GPSLong, coordinates.Longitude);
            Assert.Equal(location.Radius, coordinates.CommunityRadius);
        }
    }
}
