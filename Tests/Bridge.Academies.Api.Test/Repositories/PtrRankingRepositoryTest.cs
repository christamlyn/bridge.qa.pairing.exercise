namespace Bridge.Academies.Api.Test.Repositories
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class PtrRankingRepositoryTest : IDisposable
    {
        private readonly IWarehouseDbContext warehouseDbContext;
        private readonly Mock<ITenantService> tenantServiceMock;
        private readonly IPtrRankingRepository repository;

        public PtrRankingRepositoryTest()
        {
            this.tenantServiceMock = new Mock<ITenantService>(MockBehavior.Strict);
            this.warehouseDbContext = new InMemoryDataWarehouseContext(this.tenantServiceMock.Object);
            this.repository = new PtrRankingRepository(this.warehouseDbContext);
        }

        [Fact]
        public async Task Get_PtrRanking_Returned()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            this.warehouseDbContext.Academies.Add(
                new AcademyDimension()
                {
                    AcademyCode = "AAA",
                    AcademyDimId = 222222,
                    AcademyId = "410",
                    AcademyName = "test"
                });
            this.warehouseDbContext.GradeDailyMetrics.Add(
                new GradeDailyMetric()
                {
                    AcademyDimId = 222222,
                    DateDimId = 1111111,
                    AcademyId = 410,
                    ActiveCount = 10,
                    ActivePTR = 10,
                    AicPTR = 10,
                    NaicCount = 100,
                    AicCount = 10,
                    GradeDimId = 10
                });
            this.warehouseDbContext.Dates.Add(
                new DateDimension()
                {
                    DateDimId = 1111111,
                    DateValue = DateTime.Now,
                    Day = 1,
                    Month = 8,
                    Year = 2016
                });
            await this.warehouseDbContext.SaveChangesAsync();

            var ptrRanking = await this.repository.GetPtrRanking(410, cancellationTokenSource.Token);

            Assert.NotNull(ptrRanking);
            Assert.Equal(1, ptrRanking.Ranking);
            Assert.Equal(1, ptrRanking.TotalAcademies);
        }

        public void Dispose()
        {
            this.warehouseDbContext.Dispose();
            Mock.VerifyAll(this.tenantServiceMock);
        }
    }
}
