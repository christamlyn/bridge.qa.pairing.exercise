namespace Bridge.Academies.Api.Test.Repositories
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class KeyMetricsRepositoryTest : IDisposable
    {
        private readonly IWarehouseDbContext warehouseDbContext;
        private readonly Mock<ITenantService> tenantServiceMock;
        private readonly IKeyMetricsRepository repository;

        public KeyMetricsRepositoryTest()
        {
            this.tenantServiceMock = new Mock<ITenantService>(MockBehavior.Strict);
            this.warehouseDbContext = new InMemoryDataWarehouseContext(this.tenantServiceMock.Object);
            this.repository = new KeyMetricsRepository(this.warehouseDbContext);
        }

        [Fact]
        public async Task Get_DailyMetrics_Returned()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            this.warehouseDbContext.Academies.Add(
                new AcademyDimension()
                {
                    AcademyCode = "AAA",
                    AcademyDimId = 222222,
                    AcademyId = "410",
                    AcademyName = "test"
                });
            this.warehouseDbContext.GradeDailyMetrics.Add(
                new GradeDailyMetric()
                {
                    AcademyDimId = 222222,
                    DateDimId = 1111111,
                    AcademyId = 410,
                    ActiveCount = 10,
                    ActivePTR = 10,
                    AicPTR = 10,
                    NaicCount = 100,
                    AicCount = 10,
                    GradeDimId = 10
                });
            this.warehouseDbContext.Dates.Add(
                new DateDimension()
                {
                    DateDimId = 1111111,
                    DateValue = DateTime.Now,
                    Day = 1,
                    Month = 8,
                    Year = 2016
                });
            await this.warehouseDbContext.SaveChangesAsync();

            var dailyMetric = await this.repository.GetDailyMetrics(410, null, cancellationTokenSource.Token);

            // TODO: Come on, this is not a good test! We need to do more than just check for not null.
            Assert.NotNull(dailyMetric);
        }

        [Fact]
        public async Task Get_DailyMetrics_ReturneNull()
        {
            var cancellationTokenSource = new CancellationTokenSource();

            var dailyMetric = await this.repository.GetDailyMetrics(410, null, cancellationTokenSource.Token);

            Assert.Null(dailyMetric);
        }

        public void Dispose()
        {
            this.warehouseDbContext.Dispose();
            Mock.VerifyAll(this.tenantServiceMock);
        }
    }
}
