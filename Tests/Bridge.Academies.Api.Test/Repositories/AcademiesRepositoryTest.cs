namespace Bridge.Academies.Api.Test.Repositories
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class AcademiesRepositoryTest : IDisposable
    {
        private readonly IBridgeDbContext applicationDbContext;
        private readonly Mock<ITenantService> tenantServiceMock;
        private readonly IAcademiesRepository repository;

        public AcademiesRepositoryTest()
        {
            this.tenantServiceMock = new Mock<ITenantService>(MockBehavior.Strict);
            this.applicationDbContext = new InMemoryApplicationDbContext(this.tenantServiceMock.Object);
            this.repository = new AcademiesRepository(this.applicationDbContext);
        }

        [Fact]
        public async Task Get_IncludeLocation_AcademyReturned()
        {
            var academyLocation = new AcademyLocation()
            {
                AcademyID = 1
            };
            var region = new Region()
            {
                RegionID = 1
            };
            var cancellationTokenSource = new CancellationTokenSource();
            this.applicationDbContext.Academies.Add(
                new Academy()
                {
                    AcademyID = 1,
                    AcademyLocation = academyLocation,
                    Region = region
                });
            this.applicationDbContext.AcademyLocations.Add(academyLocation);
            this.applicationDbContext.Regions.Add(region);
            await this.applicationDbContext.SaveChangesAsync();

            var academy = await this.repository.GetAcademy(1, true, cancellationTokenSource.Token);

            Assert.NotNull(academy);
            Assert.NotNull(academy.AcademyLocation);
            Assert.NotNull(academy.Region);
        }

        [Fact]
        public async Task Get_ExcludeLocation_AcademyReturned()
        {
            this.applicationDbContext.Academies.Add(
                new Academy()
                {
                    AcademyID = 2
                });
            var cancellationTokenSource = new CancellationTokenSource();
            await this.applicationDbContext.SaveChangesAsync();

            var academy = await this.repository.GetAcademy(2, false, cancellationTokenSource.Token);

            Assert.NotNull(academy);
            Assert.Null(academy.AcademyLocation);
            Assert.Null(academy.Region);
        }

        public void Dispose()
        {
            this.applicationDbContext.Dispose();
            Mock.VerifyAll(this.tenantServiceMock);
        }
    }
}