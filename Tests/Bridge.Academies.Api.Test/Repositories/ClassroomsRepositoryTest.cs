namespace Bridge.Academies.Api.Test.Repositories
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Microsoft.EntityFrameworkCore;
    using Moq;
    using Xunit;

    public class ClassroomsRepositoryTest : IDisposable
    {
        private readonly IBridgeDbContext applicationDbContext;
        private readonly Mock<ITenantService> tenantServiceMock;
        private readonly IClassroomRepository repository;

        public ClassroomsRepositoryTest()
        {
            this.tenantServiceMock = new Mock<ITenantService>(MockBehavior.Strict);
            this.applicationDbContext = new InMemoryApplicationDbContext(this.tenantServiceMock.Object);
            this.repository = new ClassroomRepository(this.applicationDbContext);
        }

        [Fact]
        public async Task Get_AllClassrooms_ClassrommListReturned()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            this.applicationDbContext.Classrooms.Add(
                new Classroom
                {
                    ClassroomName = "TestClassroom",
                    AcademyId = 30001,
                    Active = true,
                    DateCreated = DateTime.Now,
                    DateInactivated = null
                });
            await this.applicationDbContext.SaveChangesAsync();

            var classrooms = await this.repository.GetAllClassrooms(30001, cancellationTokenSource.Token);

            Assert.NotNull(classrooms);
            Assert.Single(classrooms);
        }

        [Fact]
        public async Task Get_AllClassrooms_EmptyListReturned()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var classrooms = await this.repository.GetAllClassrooms(30001, cancellationTokenSource.Token);

            Assert.Empty(classrooms);
        }

        public void Dispose()
        {
            this.applicationDbContext.Dispose();
            Mock.VerifyAll(this.tenantServiceMock);
        }
    }
}
