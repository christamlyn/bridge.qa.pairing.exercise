namespace Bridge.Academies.Api.Test.Repositories
{
    using System;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Microsoft.EntityFrameworkCore;

    public class InMemoryApplicationDbContext : BridgeDbContext
    {
        public InMemoryApplicationDbContext(ITenantService tenantService)
            : base(tenantService)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
            optionsBuilder.UseInMemoryDatabase(Guid.NewGuid().ToString());
    }
}
