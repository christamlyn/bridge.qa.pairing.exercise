namespace Bridge.Academies.Api.Test.Repositories
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Microsoft.EntityFrameworkCore;
    using Moq;
    using Xunit;

    public class GradesRepositoryTest : IDisposable
    {
        private readonly IBridgeDbContext applicationDbContext;
        private readonly Mock<ITenantService> tenantServiceMock;
        private readonly IGradesRepository repository;

        public GradesRepositoryTest()
        {
            this.tenantServiceMock = new Mock<ITenantService>(MockBehavior.Strict);
            this.applicationDbContext = new InMemoryApplicationDbContext(this.tenantServiceMock.Object);
            this.repository = new GradesRepository(this.applicationDbContext);
        }

        [Fact]
        public async Task Get_AllGrades_GradeListReturned()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            this.applicationDbContext.Grades.Add(
                new Grades
                {
                    GradeId = 1,
                    GradeName = "Nursery",
                    GradeAbbreviation = "NU",
                    Active = true,
                    HasPlacement = false,
                    MinPlacementScore = 0,
                    MaxPlacementScore = 10
                });
            await this.applicationDbContext.SaveChangesAsync();

            var grades = await this.repository.GetAllGrades(cancellationTokenSource.Token);

            Assert.NotNull(grades);
            Assert.Single(grades);
        }

        [Fact]
        public async Task Get_AllGrades_GradeEmptyListReturned()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var grades = await this.repository.GetAllGrades(cancellationTokenSource.Token);

            Assert.Empty(grades);
        }

        public void Dispose()
        {
            this.applicationDbContext.Dispose();
            Mock.VerifyAll(this.tenantServiceMock);
        }
    }
}
