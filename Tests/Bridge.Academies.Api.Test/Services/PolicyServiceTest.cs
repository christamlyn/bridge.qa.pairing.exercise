namespace Bridge.Academies.Api.Test.Services
{
    using System;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.Settings;
    using Bridge.Framework.ExceptionHandling;
    using Microsoft.Extensions.Options;
    using Moq;
    using Xunit;

    [Trait("Category", "UnitTest")]
    public class PolicyServiceTest
    {
        private readonly Mock<IOptions<AppSettings>> appSettingsMock;
        private readonly PolicyService policyService;

        public PolicyServiceTest()
        {
            this.appSettingsMock = new Mock<IOptions<AppSettings>>(MockBehavior.Strict);

            this.policyService = new PolicyService(this.appSettingsMock.Object);
        }

        [Fact]
        public void GetDatabasePolicy_CallTwice_SameInstanceReturned()
        {
            AppSettings appSettings = new AppSettings()
            {
                Database = new DatabaseSettings()
                {
                    ExceptionHandling = new PolicyOptions()
                    {
                        DurationOfBreak = TimeSpan.FromMinutes(1),
                        ExceptionsAllowedBeforeBreaking = 1
                    }
                }
            };
            this.appSettingsMock.SetupGet(x => x.Value).Returns(appSettings);

            var policy1 = this.policyService.GetDatabasePolicy();
            var policy2 = this.policyService.GetDatabasePolicy();

            Assert.Same(policy1, policy2);
            this.VerifyAll();
        }

        private void VerifyAll()
        {
            Mock.VerifyAll(this.appSettingsMock);
        }
    }
}
