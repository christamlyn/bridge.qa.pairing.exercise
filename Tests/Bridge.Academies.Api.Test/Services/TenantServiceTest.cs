namespace Bridge.Academies.Api.Test.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.Settings;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Http.Internal;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.Extensions.Options;
    using Microsoft.Extensions.Primitives;
    using Moq;
    using Xunit;

    public class TenantServiceTest
    {
        private const string TenantIdQueryParameter = "tenantid";
        private const string BridgeDatabaseKey = "Bridge";
        private const string WarehouseDatabaseKey = "Warehouse";
        private Mock<IOptions<DatabaseSettings>> databaseSettings;
        private Mock<IHttpContextAccessor> httpContextAccessor;
        private Mock<IOptions<MultiTenancySettings>> multiTenancySettings;
        private Mock<IActionContextAccessor> actionContextAccessor;
        private TenantService tenantService;

        public TenantServiceTest()
        {
            this.databaseSettings = new Mock<IOptions<DatabaseSettings>>();
            this.httpContextAccessor = new Mock<IHttpContextAccessor>();
            this.multiTenancySettings = new Mock<IOptions<MultiTenancySettings>>();
            this.actionContextAccessor = new Mock<IActionContextAccessor>();
        }

        [Fact]
        public void GetCurrentTenantId_RetrunsTenantId()
        {
            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Query).
                Returns(new QueryCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>()
            {
                { "tenantId", new Microsoft.Extensions.Primitives.StringValues("1") }
            }));

            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Headers).
               Returns(new HeaderDictionary());

            this.multiTenancySettings.SetupGet(x => x.Value)
                .Returns(new MultiTenancySettings()
                {
                    Tenants = new List<TenantSettings>
                {
                    new TenantSettings
                    {
                        Id = 1,
                        Databases = new List<TenantDatabaseSettings>
                        {
                            new TenantDatabaseSettings
                            {
                                Catalog = "Bridge-Test",
                                Key = "Bridge",
                                Server = "Server"
                            }
                        }
                    }
                }
                });

            this.databaseSettings.SetupGet(x => x.Value)
               .Returns(new DatabaseSettings()
               {
                   BridgeConnectionString = "Data Source=localhost;Initial Catalog=BDM;Integrated Security=True;MultipleActiveResultSets=False;Min Pool Size=3;Application Name=Bridge.Academies.Api"
               });

            this.tenantService = new TenantService(
               this.databaseSettings.Object,
               this.httpContextAccessor.Object,
               this.multiTenancySettings.Object,
               this.actionContextAccessor.Object);

            var connectionString = this.tenantService.GetBridgeConnectionString();

            Assert.NotNull(connectionString);
        }

        [Fact]
        public void GetCurrentTenantId_CaseInSensitive_RetrunsTenantId()
        {
            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Query).
                Returns(new QueryCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>()
            {
                { "TeNaNtId", new Microsoft.Extensions.Primitives.StringValues("1") }
            }));

            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Headers).
                Returns(new HeaderDictionary());

            this.multiTenancySettings.SetupGet(x => x.Value)
                .Returns(new MultiTenancySettings()
                {
                    Tenants = new List<TenantSettings>
                {
                    new TenantSettings
                    {
                        Id = 1,
                        Databases = new List<TenantDatabaseSettings>
                        {
                            new TenantDatabaseSettings
                            {
                                Catalog = "Bridge-Test",
                                Key = "Bridge",
                                Server = "Server"
                            }
                        }
                    }
                }
                });

            this.databaseSettings.SetupGet(x => x.Value)
               .Returns(new DatabaseSettings()
               {
                   BridgeConnectionString = "Data Source=localhost;Initial Catalog=BDM;Integrated Security=True;MultipleActiveResultSets=False;Min Pool Size=3;Application Name=Bridge.Academies.Api"
               });

            this.tenantService = new TenantService(
               this.databaseSettings.Object,
               this.httpContextAccessor.Object,
               this.multiTenancySettings.Object,
               this.actionContextAccessor.Object);

            var connectionString = this.tenantService.GetBridgeConnectionString();

            Assert.NotNull(connectionString);
        }

        [Fact]
        public void GetCurrentTenantId_NoTenantIdMatched()
        {
            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Query).
                Returns(new QueryCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>()
            {
                { "tenantId", new Microsoft.Extensions.Primitives.StringValues("100") }
            }));

            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Headers).
               Returns(new HeaderDictionary());

            this.multiTenancySettings.SetupGet(x => x.Value)
                .Returns(new MultiTenancySettings()
                {
                    Tenants = new List<TenantSettings>
                {
                    new TenantSettings
                    {
                        Id = 1,
                        Databases = new List<TenantDatabaseSettings>
                        {
                            new TenantDatabaseSettings
                            {
                                Catalog = "Bridge-Test",
                                Key = "Bridge",
                                Server = "Server"
                            }
                        }
                    }
                }
                });

            this.databaseSettings.SetupGet(x => x.Value)
              .Returns(new DatabaseSettings()
              {
                  BridgeConnectionString = "Data Source=localhost;Initial Catalog=BDM;Integrated Security=True;MultipleActiveResultSets=False;Min Pool Size=3;Application Name=Bridge.Academies.Api"
              });

            this.tenantService = new TenantService(
               this.databaseSettings.Object,
               this.httpContextAccessor.Object,
               this.multiTenancySettings.Object,
               this.actionContextAccessor.Object);

            Assert.Throws<InvalidOperationException>(() => this.tenantService.GetBridgeConnectionString());
        }

        [Fact]
        public void GetCurrentTenantId_NoTenantQueryString()
        {
            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Query).
                Returns(new QueryCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>()
            {
                { "academy", new Microsoft.Extensions.Primitives.StringValues("1") }
            }));

            this.httpContextAccessor.SetupGet(x => x.HttpContext.Request.Headers).
               Returns(new HeaderDictionary());

            this.multiTenancySettings.SetupGet(x => x.Value)
                .Returns(new MultiTenancySettings()
                {
                    Tenants = new List<TenantSettings>
                {
                    new TenantSettings
                    {
                        Id = 1,
                        Databases = new List<TenantDatabaseSettings>
                        {
                            new TenantDatabaseSettings
                            {
                                Catalog = "Bridge-Test",
                                Key = "Bridge",
                                Server = "Server"
                            }
                        }
                    }
                }
                });

            this.databaseSettings.SetupGet(x => x.Value)
              .Returns(new DatabaseSettings()
              {
                  BridgeConnectionString = "Data Source=localhost;Initial Catalog=BDM;Integrated Security=True;MultipleActiveResultSets=False;Min Pool Size=3;Application Name=Bridge.Academies.Api"
              });

            this.tenantService = new TenantService(
               this.databaseSettings.Object,
               this.httpContextAccessor.Object,
               this.multiTenancySettings.Object,
               this.actionContextAccessor.Object);

            Assert.Throws<InvalidOperationException>(() => this.tenantService.GetBridgeConnectionString());
        }
    }
}
