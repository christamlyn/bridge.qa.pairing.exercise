﻿namespace Bridge.Academies.Api
{
    using System;
    using Newtonsoft.Json.Converters;

    [Obsolete("Do not use this. Use the default date time format which includes the UTC offset value.")]
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            this.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        }
    }
}
