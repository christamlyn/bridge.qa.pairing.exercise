namespace Bridge.Academies.Api
{
    using System.Reflection;
    using Bridge.Academies.Api.Constants;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Framework.AspNetCore;
    using Bridge.Framework.AspNetCore.AzureActiveDirectory;
    using Bridge.Framework.AspNetCore.Filters;
    using Bridge.Framework.AspNetCore.ModelBinding;
    using Bridge.Framework.AspNetCore.Status;
    using Bridge.Framework.AspNetCore.Swagger;
    using Bridge.Logging.AspNetCore;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Caching.Redis;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Startup
    {
        private readonly IConfiguration configuration;
        private readonly Assembly currentAssembly;
        private readonly IHostingEnvironment hostingEnvironment;
        private AzureActiveDirectoryOptions azureActiveDirectoryOptions;

        public Startup(IConfiguration configuration, IHostingEnvironment hostingEnvironment)
        {
            this.configuration = configuration;
            this.hostingEnvironment = hostingEnvironment;
            this.currentAssembly = typeof(Startup).GetTypeInfo().Assembly;
        }

        public virtual void ConfigureServices(IServiceCollection services) =>
            services
                .AddConfigurationServices(
                    this.configuration,
                    out this.azureActiveDirectoryOptions)
                .AddCommandServices()
                .AddRepositoryServices()
                .AddServicesServices()
                .AddTranslatorServices()
                .AddRouting(options => options.LowercaseUrls = true)
                .AddSingleton<IActionContextAccessor, ActionContextAccessor>()
                .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
                .AddScoped(x => x
                    .GetRequiredService<IUrlHelperFactory>()
                    .GetUrlHelper(x.GetRequiredService<IActionContextAccessor>().ActionContext))
                .AddScoped<RequestResponseLoggingAttribute>()
                .AddEntityFrameworkSqlServer()
                .AddDbContext<BridgeDbContext>()
                .AddDbContext<WarehouseDbContext>()
                .AddSingleton<IDistributedCache, RedisCache>()
                .AddResponseCaching()
                .AddCors(
                    options =>
                    {
                        options.AddPolicy(
                            CorsPolicyName.AllowAny,
                            builder => builder
                                .AllowAnyOrigin()
                                .AllowAnyMethod()
                                .AllowAnyHeader());
                    })
                .AddJwtOrBasicAuthentication(this.azureActiveDirectoryOptions)
                .AddAuthorization(
                    options =>
                    {
                        options.AddPolicy(
                            AuthorizationPolicyName.AcademyRead,
                            x => x
                                .RequireAuthenticatedUser()
                                .RequireClaim(ClaimType.Roles.Type, RoleName.AcademyReadAll));
                        options.AddPolicy(
                            AuthorizationPolicyName.ClassroomRead,
                            x => x
                                .RequireAuthenticatedUser()
                                .RequireClaim(ClaimType.Roles.Type, RoleName.ClassroomReadAll));
                        options.AddPolicy(
                            AuthorizationPolicyName.GradeRead,
                            x => x
                                .RequireAuthenticatedUser()
                                .RequireClaim(ClaimType.Roles.Type, RoleName.GradeReadAll));
                    })
                .AddSwaggerDefaults(
                    this.currentAssembly,
                    options =>
                    {
                        options.AddBasicAuthSecurityDefinition();
                        options.AddAzureActiveDirectorySecurityDefinition(this.azureActiveDirectoryOptions);
                        options.IncludeXmlCommentsIfExists(typeof(StatusController).GetTypeInfo().Assembly);
                    })
                .AddApiVersioning(
                    options =>
                    {
                        options.AssumeDefaultVersionWhenUnspecified = true;
                        options.ReportApiVersions = true;
                    })
                .AddApi(
                    options =>
                    {
                        options.Filters.AddService(typeof(RequestResponseLoggingAttribute));
                        options.Filters.Add(new ValidateModelStateAttribute());
                        options.OutputFormatters.RemoveType<StreamOutputFormatter>();
                        options.OutputFormatters.RemoveType<StringOutputFormatter>();
                        options.ReturnHttpNotAcceptable = true;
                        options.ValueProviderFactories
                            .AddTenantIdFromHeaderValueProviderFactory()
                            .AddDelimitedValueProviderFactory();
                    })
                .AddJsonOptions(
                    options =>
                    {
                        options.SerializerSettings.DateParseHandling = DateParseHandling.DateTimeOffset;
                        options.SerializerSettings.Converters.Add(new StringEnumConverter());
                    });

        public virtual void Configure(IApplicationBuilder application) =>
            application
                .UseResponseCaching()
                .UseCors(CorsPolicyName.AllowAny)
                .UseIf(
                    !this.hostingEnvironment.IsProduction(), x => x
                    .UseDeveloperExceptionPage()
                    .UseDatabaseErrorPage())
                .UseAuthentication()
                .UseMvc()
                .UseSwagger()
                .UseSwaggerUiDefaults(
                    options =>
                    {
                        options.ConfigureAzureActiveDirectory(this.currentAssembly, this.azureActiveDirectoryOptions);
                    });
    }
}