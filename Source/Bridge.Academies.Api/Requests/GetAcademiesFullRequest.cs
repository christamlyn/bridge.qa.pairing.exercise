﻿namespace Bridge.Academies.Api.Requests
{
    using System;

    public class GetAcademiesFullRequest
    {
        public string AcademyIds { get; set; }

        public bool? IsActive { get; set; }

        public DateTimeOffset? ModifiedSince { get; set; }
    }
}
