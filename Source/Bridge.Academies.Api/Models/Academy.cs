namespace Bridge.Academies.Api.Models
{
    using System;
    using System.Collections.Generic;

    public class Academy
    {
        public int AcademyID { get; set; }

        public string AcademyName { get; set; }

        public string AcademyCode { get; set; }

        public virtual AcademyLocation AcademyLocation { get; set; }

        public string Location { get; set; }

        public string AcademyManager { get; set; }

        public string AcademyManagerContactPhone { get; set; }

        public decimal? BalanceAllowance { get; set; }

        public string DeregistrationPeriod { get; set; }

        public int ActiveInteger { get; set; }

        public bool Active
        {
            get { return this.ActiveInteger != 0; }
            set { this.ActiveInteger = value ? 1 : 0; }
        }

        public string CurrentTerm { get; set; }

        public int? CurrentYear { get; set; }

        public string AccountCode { get; set; }

        public string FeeCategory { get; set; }

        public int WageCategory { get; set; }

        public DateTime LaunchDate { get; set; }

        public bool? BillingSMSEnabled { get; set; }

        public int? RegionID { get; set; }

        public virtual Region Region { get; set; }

        public string ReligiousEducation { get; set; }

        public string ShortCode { get; set; }

        public List<Classroom> Classrooms { get; set; }
    }
}
