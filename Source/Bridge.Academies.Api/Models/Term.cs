﻿namespace Bridge.Academies.Api.Models
{
    public enum Term
    {
        Current,
        Last,
        PreviousToLast
    }
}
