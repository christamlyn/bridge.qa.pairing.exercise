﻿namespace Bridge.Academies.Api.Models
{
    using System;

    public class PtrRanking
    {
        public long AcademyId { get; set; }

        // Denotes academy rank based on AicPTR
        public int Ranking { get; set; }

        // Denotes total number of academies
        public int TotalAcademies { get; set; }

        public int? AicPTR { get; set; }
    }
}
