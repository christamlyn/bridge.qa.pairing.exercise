namespace Bridge.Academies.Api.Models
{
    using System;

    public class Classroom
    {
        public int ClassroomId { get; set; }

        public int AcademyId { get; set; }

        public int GradeId { get; set; }

        public string ClassroomName { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? LastUpdated { get; set; }

        public bool Active { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? DateInactivated { get; set; }

        public virtual Academy Academies { get; set; }

        public virtual Grades Grades { get; set; }
    }
}
