﻿namespace Bridge.Academies.Api.Models
{
    using System;

    public class PupilDailyMetric
    {
        public long PupilDimId { get; set; }

        public long DateDimId { get; set; }

        public long AcademyDimId { get; set; }

        public int PupilStatusDimId { get; set; }
    }
}
