﻿namespace Bridge.Academies.Api.Models
{
    using System;

    public class GradeDailyMetric
    {
        public long AcademyDimId { get; set; }

        public long AcademyId { get; set; }

        public int GradeDimId { get; set; }

        public long DateDimId { get; set; }

        public int? ActiveCount { get; set; }

        public int? AicCount { get; set; }

        public int? ActivePTR { get; set; }

        public int? AicPTR { get; set; }

        public int? NaicCount { get; set; }

        public DateTime UpdateDate { get; set; }

        public DateDimension Date { get; set; }
    }
}
