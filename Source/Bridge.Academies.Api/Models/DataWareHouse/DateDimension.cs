﻿namespace Bridge.Academies.Api.Models
{
    using System;

    public class DateDimension
    {
        public long DateDimId { get; set; }

        public DateTime DateValue { get; set; }

        public long Year { get; set; }

        public long Month { get; set; }

        public long Day { get; set; }
    }
}