﻿namespace Bridge.Academies.Api.Models
{
    public class AcademicTermDimension
    {
        public int AcademicTermDimId { get; set; }

        public int TermId { get; set; }

        public long Term { get; set; }

        public int TermStartDateDimId { get; set; }
    }
}
