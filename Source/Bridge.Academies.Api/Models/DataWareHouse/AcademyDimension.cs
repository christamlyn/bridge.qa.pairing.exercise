﻿namespace Bridge.Academies.Api.Models
{
    public class AcademyDimension
    {
        public int AcademyDimId { get; set; }

        public string AcademyId { get; set; }

        public string AcademyName { get; set; }

        public string AcademyCode { get; set; }
    }
}
