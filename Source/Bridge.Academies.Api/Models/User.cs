﻿namespace Bridge.Academies.Api.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class User
    {
        public string Username { get; set; }

        public int AcademyId { get; set; }

        public virtual Academy Academy { get; set; }

        public bool Active { get; set; }
    }
}
