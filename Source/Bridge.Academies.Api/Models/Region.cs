namespace Bridge.Academies.Api.Models
{
    using System;

    public class Region
    {
        public int Id { get; set; }

        public int RegionID { get; set; }

        public string Country { get; set; }

        public string LevelOne { get; set; }

        public string LevelTwo { get; set; }

        public string LevelThree { get; set; }

        public string LevelFour { get; set; }

        public string LevelFive { get; set; }

        public string LevelSix { get; set; }

        public string LevelSeven { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? LastUpdated { get; set; }
    }
}
