namespace Bridge.Academies.Api.ViewModelSchemaFilters
{
    using System;
    using Bridge.Academies.Api.ViewModels;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;

    public class NearestAcademyFullSchemaFilter : ISchemaFilter
    {
        public void Apply(Schema model, SchemaFilterContext context)
        {
            model.Default = new NearestAcademyFull()
            {
                AcademyId = 1,
                Active = true,
                Code = "BUGUMBA-IC",
                Distance = 1000,
                RegionId = 1,
                LaunchDate = new DateTimeOffset(new DateTime(2015, 1, 1)),
                Location = new Location()
                {
                    Coordinates = new Coordinates()
                    {
                        Latitude = 0.62876,
                        Longitude = 33.47682,
                        CommunityRadius = 1.22
                    },
                    Country = "Uganda",
                    Level1 = "Eastern",
                    Level2 = "Busoga",
                    Level3 = "Iganga",
                    Level4 = "Kigulu",
                    Level5 = "Bulamagi",
                    Level6 = "Bugumba",
                    Level7 = "Bugumba"
                },
                Name = "Bugumba-IC",
                ShortCode = "BI",
                Phone = "2313424324"
            };
        }
    }
}
