namespace Bridge.Academies.Api.ViewModelSchemaFilters
{
    using Bridge.Academies.Api.ViewModels;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;

    public class AcademyLocationSchemaFilter : ISchemaFilter
    {
        public void Apply(Schema model, SchemaFilterContext context)
        {
            model.Default = new AcademyLocation()
            {
                AcademyId = 1,
                Coordinates = new Coordinates()
                {
                    Latitude = 0.62876,
                    Longitude = 33.47682,
                    CommunityRadius = 1.22
                },
                Country = "Uganda",
                Level1 = "Eastern",
                Level2 = "Busoga",
                Level3 = "Iganga",
                Level4 = "Kigulu",
                Level5 = "Bulamagi",
                Level6 = "Bugumba",
                Level7 = "Bugumba"
            };
        }
    }
}
