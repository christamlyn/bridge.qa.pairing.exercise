namespace Bridge.Academies.Api.ViewModelSchemaFilters
{
    using System;
    using Bridge.Academies.Api.ViewModels;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;

    public class AcademySchemaFilter : ISchemaFilter
    {
        public void Apply(Schema model, SchemaFilterContext context)
        {
            model.Default = new Academy()
            {
                AcademyId = 1,
                Active = true,
                Code = "BUGUMBA-IC",
                RegionId = 1,
                LaunchDate = new DateTimeOffset(new DateTime(2015, 1, 1)),
                Name = "Bugumba-IC",
                ShortCode = "BI",
                Phone = "213213213"
            };
        }
    }
}
