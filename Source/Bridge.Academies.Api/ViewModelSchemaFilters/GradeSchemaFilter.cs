namespace Bridge.Academies.Api.ViewModelSchemaFilters
{
    using Bridge.Academies.Api.ViewModels;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerGen;

    public class GradeSchemaFilter : ISchemaFilter
    {
        public void Apply(Schema model, SchemaFilterContext context)
        {
            model.Default = new Grade()
            {
                Code = "KI",
                GradeId = 1,
                Name = "Kindergarten",
                Order = 3,
                MaxAgeOffset = 371,
                MinAgeOffset = 120
            };
        }
    }
}
