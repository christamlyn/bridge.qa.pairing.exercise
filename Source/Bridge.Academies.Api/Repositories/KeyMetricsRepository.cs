namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.EntityFrameworkCore;

    public class KeyMetricsRepository : IKeyMetricsRepository
    {
        private readonly IWarehouseDbContext context;

        public KeyMetricsRepository(IWarehouseDbContext context)
        {
            this.context = context;
        }

        public async Task<GradeDailyMetric> GetDailyMetrics(
            int academyId,
            DateTimeOffset? filterDate,
            CancellationToken cancellationToken)
        {
            var academyIdString = academyId.ToString();

            DateTime date;
            if (filterDate.HasValue)
            {
                date = filterDate.Value.DateTime;
            }
            else
            {
                date = await this.GetProcessedDate(academyIdString, cancellationToken);
            }

            var result = await this.context.GradeDailyMetrics
                .Join(
                    this.context.Academies,
                    x => x.AcademyDimId,
                    y => y.AcademyDimId,
                    (x, y) => new { Academies = y, DailyMetrics = x })
                .Join(
                    this.context.Dates,
                    x => x.DailyMetrics.DateDimId,
                    y => y.DateDimId,
                    (x, y) => new { x.DailyMetrics, x.Academies, y.DateValue, y.DateDimId })
                 .Where(x => x.Academies.AcademyId.Equals(academyIdString) && x.DateValue.Equals(date))
                 .GroupBy(x => new { x.DateDimId, x.DateValue, x.DailyMetrics.UpdateDate })
                 .Select(
                    x => new
                    {
                        x.Key.DateValue,
                        x.Key.UpdateDate,
                        ActiveCount = (int?)x.Average(p => p.DailyMetrics.ActiveCount),
                        AicCount = (int?)x.Average(p => p.DailyMetrics.AicCount),
                        ActivePTR = (int?)x.Average(p => p.DailyMetrics.ActivePTR),
                        AicPTR = (int?)x.Average(p => p.DailyMetrics.AicPTR),
                        TotalActiveCount = (int?)x.Sum(p => p.DailyMetrics.ActiveCount),
                        TotalAicCount = (int?)x.Sum(p => p.DailyMetrics.AicCount)
                    })
                 .FirstOrDefaultAsync(cancellationToken);

            var response = new GradeDailyMetric();
            if (result != null)
            {
                response.Date = new DateDimension
                {
                    DateValue = result.DateValue
                };
                response.ActiveCount = result.ActiveCount;
                response.ActivePTR = result.ActivePTR;
                response.AicCount = result.TotalAicCount;
                response.AicPTR = result.AicPTR;
                response.NaicCount = result.TotalActiveCount - result.TotalAicCount;
                response.UpdateDate = result.UpdateDate;

                return response;
            }

            return null;
        }

        private Task<DateTime> GetProcessedDate(string academyId, CancellationToken cancellationToken)
        {
            var date =
                (from dates in this.context.Dates
                 join metrics in this.context.GradeDailyMetrics
                 on dates.DateDimId equals metrics.DateDimId
                 join ac in this.context.Academies
                 on metrics.AcademyDimId equals ac.AcademyDimId
                 orderby dates.DateDimId descending
                 where ac.AcademyId == academyId
                 select dates.DateValue)
                .FirstOrDefaultAsync(cancellationToken);
            return date;
        }
    }
}
