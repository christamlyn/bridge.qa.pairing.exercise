namespace Bridge.Academies.Api.Repositories
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;

    public interface IProbableNaicsRepository
    {
        Task<ProbableNaics> GetProbableNaics(int academyId, CancellationToken cancellationToken);
    }
}
