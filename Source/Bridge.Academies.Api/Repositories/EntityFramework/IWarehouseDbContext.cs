namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;

    public interface IWarehouseDbContext : IDbContext
    {
        DbSet<GradeDailyMetric> GradeDailyMetrics { get; set; }

        DbSet<AcademyDimension> Academies { get; set; }

        DbSet<DateDimension> Dates { get; set; }

        DbSet<PupilDailyMetric> PupilDailyMetrics { get; set; }

        DbSet<AcademicTermDimension> AcademicTerms { get; set; }

        DbSet<PupilDimension> Pupils { get; set; }
    }
}
