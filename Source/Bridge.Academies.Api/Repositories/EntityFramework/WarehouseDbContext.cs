namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Services;
    using Microsoft.EntityFrameworkCore;

    public partial class WarehouseDbContext : DbContext, IWarehouseDbContext
    {
        private readonly ITenantService tenantService;

        public WarehouseDbContext(ITenantService tenantService)
        {
            this.tenantService = tenantService;

            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public virtual DbSet<GradeDailyMetric> GradeDailyMetrics { get; set; }

        public virtual DbSet<AcademyDimension> Academies { get; set; }

        public virtual DbSet<DateDimension> Dates { get; set; }

        public virtual DbSet<PupilDailyMetric> PupilDailyMetrics { get; set; }

        public virtual DbSet<AcademicTermDimension> AcademicTerms { get; set; }

        public virtual DbSet<PupilDimension> Pupils { get; set; }

        protected ITenantService TenantService => this.tenantService;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = this.tenantService.GetWarehouseConnectionString();
            optionsBuilder.UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AcademyDimensionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DateDimensionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new GradeDailyMetricsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PupilDailyMetricsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AcademicTermDimensionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new PupilDimensionEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
