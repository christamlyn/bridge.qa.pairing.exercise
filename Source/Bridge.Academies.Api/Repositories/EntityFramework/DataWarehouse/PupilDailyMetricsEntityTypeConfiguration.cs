namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PupilDailyMetricsEntityTypeConfiguration : IEntityTypeConfiguration<PupilDailyMetric>
    {
        public void Configure(EntityTypeBuilder<PupilDailyMetric> builder)
        {
            builder.ToTable("F_PUPIL_DAILY_METRICS");
            builder.HasKey(t => new { t.PupilDimId, t.AcademyDimId, t.DateDimId });

            builder.Property(t => t.PupilDimId).HasColumnName("PUPIL_DIM_ID");
            builder.Property(t => t.DateDimId).HasColumnName("DATE_DIM_ID");
            builder.Property(t => t.AcademyDimId).HasColumnName("ACADEMY_DIM_ID");
            builder.Property(t => t.PupilStatusDimId).HasColumnName("PUPIL_STATUS_DIM_ID");
        }
    }
}
