namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AcademicTermDimensionEntityTypeConfiguration : IEntityTypeConfiguration<AcademicTermDimension>
    {
        public void Configure(EntityTypeBuilder<AcademicTermDimension> builder)
        {
            builder.HasKey(x => x.AcademicTermDimId);
            builder.Property(t => t.AcademicTermDimId).HasColumnName("ACADEMIC_TERM_DIM_ID");
            builder.Property(t => t.TermId).HasColumnName("TERM_ID");
            builder.Property(t => t.Term).HasColumnName("TERM");
            builder.Property(t => t.TermStartDateDimId).HasColumnName("TERM_START_DATE_DIM_ID");
            builder.ToTable("D_ACADEMIC_TERM");
        }
    }
}
