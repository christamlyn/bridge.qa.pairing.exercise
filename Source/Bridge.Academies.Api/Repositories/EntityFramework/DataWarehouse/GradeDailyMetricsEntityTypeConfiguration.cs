namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class GradeDailyMetricsEntityTypeConfiguration : IEntityTypeConfiguration<GradeDailyMetric>
    {
        public void Configure(EntityTypeBuilder<GradeDailyMetric> builder)
        {
            builder.ToTable("F_GRADE_DAILY_METRICS");
            builder.HasKey(t => new { t.AcademyDimId, t.GradeDimId, t.DateDimId });

            builder.Property(t => t.AcademyDimId).HasColumnName("ACADEMY_DIM_ID");
            builder.Property(t => t.GradeDimId).HasColumnName("GRADE_DIM_ID");
            builder.Property(t => t.DateDimId).HasColumnName("DATE_DIM_ID");
            builder.Property(t => t.ActiveCount).HasColumnName("ACTIVE_COUNT");
            builder.Property(t => t.ActivePTR).HasColumnName("ACTIVE_PTR");
            builder.Property(t => t.AicCount).HasColumnName("AIC_COUNT");
            builder.Property(t => t.AicPTR).HasColumnName("AIC_PTR");
            builder.Property(t => t.UpdateDate).HasColumnName("UPDATE_DT");
            builder.Ignore(x => x.AcademyId);
            builder.Ignore(x => x.NaicCount);
        }
    }
}
