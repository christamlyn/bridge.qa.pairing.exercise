namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PupilDimensionEntityTypeConfiguration : IEntityTypeConfiguration<PupilDimension>
    {
        public void Configure(EntityTypeBuilder<PupilDimension> builder)
        {
            builder.HasKey(x => x.PupilDimId);
            builder.Property(t => t.PupilDimId).HasColumnName("Pupil_Dim_Id");
            builder.Property(t => t.PupilId).HasColumnName("PupilId");
            builder.Property(t => t.Active).HasColumnName("Active");
            builder.ToTable("D_PUPIL");
        }
    }
}
