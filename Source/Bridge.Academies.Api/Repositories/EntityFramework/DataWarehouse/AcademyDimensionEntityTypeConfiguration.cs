namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AcademyDimensionEntityTypeConfiguration : IEntityTypeConfiguration<AcademyDimension>
    {
        public void Configure(EntityTypeBuilder<AcademyDimension> builder)
        {
            builder.HasKey(x => x.AcademyDimId);
            builder.Property(t => t.AcademyDimId).HasColumnName("Academy_Dim_ID");
            builder.ToTable("D_ACADEMY");
        }
    }
}
