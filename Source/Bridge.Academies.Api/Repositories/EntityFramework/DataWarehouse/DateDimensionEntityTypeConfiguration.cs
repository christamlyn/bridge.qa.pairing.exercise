namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class DateDimensionEntityTypeConfiguration : IEntityTypeConfiguration<DateDimension>
    {
        public void Configure(EntityTypeBuilder<DateDimension> builder)
        {
            builder.ToTable("D_Date");
            builder.HasKey(t => t.DateDimId);

            builder.Property(t => t.DateDimId).HasColumnName("DATE_DIM_ID");
            builder.Property(t => t.DateValue).HasColumnName("DateValue_Date");
        }
    }
}
