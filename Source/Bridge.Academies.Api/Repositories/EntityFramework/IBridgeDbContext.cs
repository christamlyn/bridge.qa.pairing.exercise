namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;

    public interface IBridgeDbContext : IDbContext
    {
        DbSet<Academy> Academies { get; set; }

        DbSet<AcademyLocation> AcademyLocations { get; set; }

        DbSet<Region> Regions { get; set; }

        DbSet<Grades> Grades { get; set; }

        DbSet<Classroom> Classrooms { get; set; }

        DbSet<User> Users { get; set; }
    }
}
