namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class RegionEntityTypeConfiguration : IEntityTypeConfiguration<Region>
    {
        public void Configure(EntityTypeBuilder<Region> builder)
        {
            builder.HasKey(x => x.RegionID);

            builder.Property(x => x.Country)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.LevelOne)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.LevelTwo)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.LevelThree)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.LevelFour)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.LevelFive)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.LevelSix)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.LevelSeven)
                .HasMaxLength(50)
                .IsUnicode(false);
        }
    }
}
