namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AcademyLocationEntityTypeConfiguration : IEntityTypeConfiguration<AcademyLocation>
    {
        public void Configure(EntityTypeBuilder<AcademyLocation> builder)
        {
            builder.ToTable("AcademyLocations");

            builder.HasKey(x => x.AcademyID);

            builder.Property(x => x.GPSLat);

            builder.Property(x => x.GPSLong);

            builder.Property(x => x.Radius);
        }
    }
}
