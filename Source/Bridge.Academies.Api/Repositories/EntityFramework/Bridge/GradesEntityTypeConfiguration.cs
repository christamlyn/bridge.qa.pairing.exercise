namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class GradesEntityTypeConfiguration : IEntityTypeConfiguration<Grades>
    {
        public void Configure(EntityTypeBuilder<Grades> builder)
        {
            builder.HasKey(x => x.GradeId);

            builder.Property(x => x.GradeName)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.GradeName)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.GradeAbbreviation)
                .HasMaxLength(10)
                .IsUnicode(false);

            builder.Property(x => x.HasPlacement)
                .HasColumnType("bit")
                .IsRequired();

            builder.Property(x => x.MinPlacementScore)
                .IsRequired();

            builder.Property(x => x.MaxPlacementScore)
               .IsRequired();

            builder.Property(x => x.Active)
                .IsRequired();

            builder.Property(x => x.MaxAgeOffset)
                .IsRequired();
        }
    }
}
