namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ClassroomsEntityTypeConfiguration : IEntityTypeConfiguration<Classroom>
    {
        public void Configure(EntityTypeBuilder<Classroom> builder)
        {
            builder.HasKey(x => x.AcademyId);

            builder.Property(x => x.Active)
                .IsRequired();

            builder.Property(x => x.ClassroomId)
                .IsRequired();

            builder.Property(x => x.ClassroomName)
                .HasColumnName("Classroom")
                .HasMaxLength(1)
                .IsRequired()
                .IsUnicode(false);
        }
    }
}
