namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class AcademyEntityTypeConfiguration : IEntityTypeConfiguration<Academy>
    {
        public void Configure(EntityTypeBuilder<Academy> builder)
        {
            builder.HasKey(x => x.AcademyID);

            builder.Property(x => x.AcademyName)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(x => x.ShortCode)
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(x => x.AcademyCode)
                .HasMaxLength(150)
                .IsUnicode(false);

            builder.Property(x => x.Location)
                 .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.AcademyManager)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.AcademyManagerContactPhone)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.ActiveInteger)
                .HasColumnName("Active");

            builder.Ignore(x => x.Active);

            builder.Property(x => x.BalanceAllowance)
                .HasColumnType("money(19,4)");

            builder.Property(x => x.DeregistrationPeriod)
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(x => x.CurrentTerm)
                .HasMaxLength(2)
                .IsUnicode(false);

            builder.Property(x => x.AccountCode)
                .HasMaxLength(20)
                .IsUnicode(false);

            builder.Property(x => x.FeeCategory)
                .HasMaxLength(20)
                .IsUnicode(false);

            builder.Property(x => x.LaunchDate)
                .HasColumnType("date");

            builder.Property(x => x.ReligiousEducation)
                .HasMaxLength(20)
                .IsUnicode(false);

            builder.Property(x => x.RegionID);
        }
    }
}
