namespace Bridge.Academies.Api.Repositories.EntityFramework
{
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Services;
    using Microsoft.EntityFrameworkCore;

    public partial class BridgeDbContext : DbContext, IBridgeDbContext
    {
        private readonly ITenantService tenantService;

        public BridgeDbContext(ITenantService tenantService)
        {
            this.tenantService = tenantService;

            this.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public virtual DbSet<Academy> Academies { get; set; }

        public virtual DbSet<AcademyLocation> AcademyLocations { get; set; }

        public virtual DbSet<Classroom> Classrooms { get; set; }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Grades> Grades { get; set; }

        public virtual DbSet<Region> Regions { get; set; }

        protected ITenantService TenantService => this.tenantService;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = this.tenantService.GetBridgeConnectionString();
            optionsBuilder.UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AcademyEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new AcademyLocationEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ClassroomsEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new GradesEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new RegionEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UsersEntityTypeConfiguration());

            BuildRelationships(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }

        private static void BuildRelationships(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Academy>()
                .HasOne(x => x.Region)
                .WithMany()
                .HasForeignKey(x => x.RegionID);

            modelBuilder
               .Entity<Academy>()
               .HasOne(x => x.AcademyLocation)
               .WithOne();

            modelBuilder
                .Entity<Classroom>()
                .HasOne(x => x.Grades)
                .WithMany(x => x.Classrooms)
                .HasForeignKey(x => x.GradeId);

            modelBuilder
                .Entity<Classroom>()
                .HasOne(x => x.Academies)
                .WithMany(x => x.Classrooms)
                .HasForeignKey(x => x.AcademyId);

            modelBuilder
              .Entity<User>()
              .HasOne(x => x.Academy)
              .WithOne();
        }
    }
}
