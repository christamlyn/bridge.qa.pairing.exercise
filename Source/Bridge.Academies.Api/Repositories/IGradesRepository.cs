namespace Bridge.Academies.Api.Repositories
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;

    public interface IGradesRepository
    {
        Task<List<Grades>> GetAllGrades(CancellationToken cancellationToken);
    }
}
