namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.EntityFrameworkCore;

    public class PtrRankingRepository : IPtrRankingRepository
    {
        private readonly IWarehouseDbContext context;

        public PtrRankingRepository(IWarehouseDbContext context)
        {
            this.context = context;
        }

        public async Task<PtrRanking> GetPtrRanking(int academyId, CancellationToken cancellationToken)
        {
            var keyMetrics = await this.GetProcessedKeyMetrics(academyId, cancellationToken);
            return keyMetrics
                .Where(x => x.AcademyId == academyId)
                .Select(
                    x => new PtrRanking
                    {
                        AcademyId = academyId,
                        Ranking = keyMetrics.Where(y => y.AicPTR > x.AicPTR).Count() + 1,
                        TotalAcademies = keyMetrics.Count,
                        AicPTR = x.AicPTR
                    })
                .FirstOrDefault();
        }

        private async Task<IList<GradeDailyMetric>> GetProcessedKeyMetrics(int academyId, CancellationToken cancellationToken)
        {
            DateTime date = await this.GetProcessedDate(academyId, cancellationToken);
            var result = await this.context.GradeDailyMetrics
                .Join(
                    this.context.Academies,
                    x => x.AcademyDimId,
                    y => y.AcademyDimId,
                    (x, y) => new { Academies = y, DailyMetrics = x })
                .Join(
                    this.context.Dates,
                    x => x.DailyMetrics.DateDimId,
                    y => y.DateDimId,
                    (x, y) => new { x.DailyMetrics, x.Academies, y.DateValue, y.DateDimId, x.Academies.AcademyId })
                .Where(x => x.DateValue == date)
                .GroupBy(x => new { x.DateDimId, x.DateValue, x.AcademyId })
                .Select(
                    x => new
                    {
                        x.Key.AcademyId,
                        AicPTR = x.Average(p => p.DailyMetrics.AicPTR)
                    })
                .ToListAsync(cancellationToken);
            return result
                .Select(
                    x => new GradeDailyMetric()
                    {
                        AcademyId = Convert.ToInt64(x.AcademyId),
                        AicPTR = (int?)x.AicPTR
                    })
                .ToList();
        }

        private Task<DateTime> GetProcessedDate(int academyId, CancellationToken cancellationToken)
        {
            var academyIdString = academyId.ToString();
            var date =
                (from dates in this.context.Dates
                join metrics in this.context.GradeDailyMetrics
                on dates.DateDimId equals metrics.DateDimId
                join ac in this.context.Academies
                on metrics.AcademyDimId equals ac.AcademyDimId
                orderby dates.DateDimId descending
                where ac.AcademyId == academyIdString
                select dates.DateValue)
                .FirstOrDefaultAsync(cancellationToken);
            return date;
        }
    }
}
