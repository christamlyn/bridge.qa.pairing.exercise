namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.EntityFrameworkCore;

    public class AcademiesRepository : IAcademiesRepository
    {
        private readonly IBridgeDbContext applicationDbContext;

        public AcademiesRepository(IBridgeDbContext applicationDbContext) =>
            this.applicationDbContext = applicationDbContext;

        public Task<Academy> GetAcademy(
            int academyId,
            bool includeLocation,
            CancellationToken cancellationToken) =>
            GetAcademies(this.applicationDbContext, includeLocation, cancellationToken)
                .FirstOrDefaultAsync(x => x.AcademyID == academyId, cancellationToken);

        public Task<List<Academy>> GetAllAcademies(
            IEnumerable<int> academyIds,
            bool? isActive,
            bool includeLocation,
            DateTimeOffset? modifiedSince,
            CancellationToken cancellationToken) =>
            GetAcademies(this.applicationDbContext, includeLocation, cancellationToken)
                .Where(x => academyIds == null || academyIds.Count() == 0 || academyIds.Contains(x.AcademyID))
                .Where(x => isActive == null || isActive.Value == x.Active)
                .Where(x => modifiedSince == null || modifiedSince.Value.DateTime < x.LaunchDate)
                .ToListAsync(cancellationToken);

        private static IQueryable<Academy> GetAcademies(
            IBridgeDbContext context,
            bool includeLocation,
            CancellationToken cancellationToken)
        {
            IQueryable<Academy> academies;
            if (includeLocation)
            {
                academies = context
                    .Academies
                    .GroupJoin(
                        context.AcademyLocations,
                        x => x.AcademyID,
                        x => x.AcademyID,
                        (x, y) => new { Academies = x, AcademyLocations = y.FirstOrDefault() })
                    .Select(x => x.Academies)
                    .GroupJoin(
                        context.Regions,
                        x => x.RegionID,
                        x => x.RegionID,
                        (x, y) => new { Academies = x, Regions = y.FirstOrDefault() })
                    .Select(x => x.Academies)
                    .Include(x => x.AcademyLocation)
                    .Include(x => x.Region);
            }
            else
            {
                academies = context.Academies;
            }

            return academies;
        }
    }
}
