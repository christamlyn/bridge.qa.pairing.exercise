namespace Bridge.Academies.Api.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.EntityFrameworkCore;

    public class GradesRepository : IGradesRepository
    {
        private readonly IBridgeDbContext applicationDbContext;

        public GradesRepository(IBridgeDbContext applicationDbContext) =>
            this.applicationDbContext = applicationDbContext;

        public Task<List<Grades>> GetAllGrades(CancellationToken cancellationToken) =>
            this.applicationDbContext.Grades
                .Where(x => x.Active)
                .OrderBy(x => x.SortOrder)
                .ToListAsync(cancellationToken);
    }
}
