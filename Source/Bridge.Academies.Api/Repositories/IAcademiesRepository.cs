namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;

    public interface IAcademiesRepository
    {
        Task<Academy> GetAcademy(int academyId, bool includeLocation, CancellationToken cancellationToken);

        Task<List<Academy>> GetAllAcademies(
            IEnumerable<int> academyIds,
            bool? isActive,
            bool includeLocation,
            DateTimeOffset? modifiedSince,
            CancellationToken cancellationToken);
    }
}
