namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.EntityFrameworkCore;

    public class ProbableNaicsRepository : IProbableNaicsRepository
    {
        private readonly IWarehouseDbContext context;

        public ProbableNaicsRepository(IWarehouseDbContext context)
        {
            this.context = context;
        }

        public async Task<ProbableNaics> GetProbableNaics(int academyId, CancellationToken cancellationToken)
        {
            var result = (await this.GetProbableNaics(academyId, await this.GetAcademyTermDate(Term.Current, cancellationToken), Status.NAIC, cancellationToken))
                .Union(await this.GetProbableNaics(academyId, await this.GetAcademyTermDate(Term.Last, cancellationToken), Status.NAIC, cancellationToken));
            var response = new ProbableNaics() { PupilId = result.ToList() };
            return response;
        }

        private Task<List<long>> GetProbableNaics(
            int academyId,
            long dateDimId,
            Status status,
            CancellationToken cancellationToken)
        {
            var academyIdString = academyId.ToString();
           return this.context.PupilDailyMetrics
                .Join(
                this.context.Academies,
                x => x.AcademyDimId,
                y => y.AcademyDimId,
                (x, y) => new { x, y })
                .Join(
                this.context.AcademicTerms,
                x => x.x.DateDimId,
                y => y.TermStartDateDimId,
                (x, y) => new { x, y })
                .Join(
                this.context.Pupils,
                x => x.x.x.PupilDimId,
                y => y.PupilDimId,
                (x, y) => new { x, y }).Where(x => x.x.x.x.DateDimId.Equals(dateDimId) && x.x.x.x.PupilStatusDimId.Equals((int)status) && x.x.x.y.AcademyId.Equals(academyIdString))
                 .Select(
                    x => x.y.PupilId).ToListAsync(cancellationToken);
        }

        private Task<int> GetAcademyTermDate(Term term, CancellationToken cancellationToken)
        {
            if (term == Term.Current)
            {
                var date = (from dates in this.context.AcademicTerms
                            orderby dates.TermStartDateDimId descending
                            select dates.TermStartDateDimId).FirstOrDefaultAsync(cancellationToken);
                return date;
            }
            else if (term == Term.Last)
            {
                var date = (from dates in this.context.AcademicTerms
                            orderby dates.TermStartDateDimId descending
                            select dates.TermStartDateDimId).FirstOrDefault();

                var d = this.context
                    .AcademicTerms
                    .Where(x => x.TermStartDateDimId != date)
                    .OrderByDescending(x => x.TermStartDateDimId)
                    .Select(x => x.TermStartDateDimId)
                    .FirstOrDefaultAsync(cancellationToken);
                return d;
            }

            return null;
        }
    }
}
