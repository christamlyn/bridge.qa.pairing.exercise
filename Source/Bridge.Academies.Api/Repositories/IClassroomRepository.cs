namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;

    public interface IClassroomRepository
    {
        Task<List<Classroom>> GetAllClassrooms(int academyId, CancellationToken cancellationToken);
    }
}
