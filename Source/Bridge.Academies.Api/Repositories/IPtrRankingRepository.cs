namespace Bridge.Academies.Api.Repositories
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;

    public interface IPtrRankingRepository
    {
        Task<PtrRanking> GetPtrRanking(int academyId, CancellationToken cancellationToken);
    }
}
