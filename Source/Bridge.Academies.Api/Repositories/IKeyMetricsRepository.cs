namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;

    public interface IKeyMetricsRepository
    {
        Task<GradeDailyMetric> GetDailyMetrics(
            int academyId,
            DateTimeOffset? filterDate,
            CancellationToken cancellationToken);
    }
}
