namespace Bridge.Academies.Api.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Models;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Microsoft.EntityFrameworkCore;

    public class ClassroomRepository : IClassroomRepository
    {
        private readonly IBridgeDbContext applicationDbContext;

        public ClassroomRepository(IBridgeDbContext applicationDbContext) =>
            this.applicationDbContext = applicationDbContext;

        public Task<List<Classroom>> GetAllClassrooms(int academyId, CancellationToken cancellationToken) =>
            this.applicationDbContext
                .Classrooms
                .ToListAsync(cancellationToken);
    }
}
