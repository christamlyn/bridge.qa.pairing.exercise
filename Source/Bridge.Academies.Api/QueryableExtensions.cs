﻿namespace Bridge.Academies.Api
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using Microsoft.EntityFrameworkCore;

    public static class QueryableExtensions
    {
        public static IQueryable<TEntity> IncludeIf<TEntity, TProperty>(
            this IQueryable<TEntity> source,
            Expression<Func<TEntity, TProperty>> navigationPropertyPath,
            bool condition)
            where TEntity : class
        {
            if (condition)
            {
                return source.Include(navigationPropertyPath);
            }

            return source;
        }
    }
}