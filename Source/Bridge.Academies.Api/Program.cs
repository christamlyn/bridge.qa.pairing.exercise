namespace Bridge.Academies.Api
{
    using System.IO;
    using Bridge.Framework.AspNetCore;
    using Bridge.Logging.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Serilog;

    public static class Program
    {
        public static int Main(string[] args) => BuildWebHost(args).LogAndRun();

        public static IWebHost BuildWebHost(string[] args) =>
            new WebHostBuilder()
                .UseKestrel(options => options.AddServerHeader = false)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                    config.AddBridgeDefaultConfiguration(hostingContext.HostingEnvironment, args))
                .UseConfiguration(new ConfigurationBuilder().AddCommandLine(args).Build())
                .UseIISIntegration()
                .UseDefaultServiceProvider((context, options) =>
                    options.ValidateScopes = context.HostingEnvironment.IsDevelopment())
                .UseStartup<Startup>()
                .UseSerilog()
                .Build();
    }
}