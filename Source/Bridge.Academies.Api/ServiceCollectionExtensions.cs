namespace Bridge.Academies.Api
{
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Repositories.EntityFramework;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.Settings;
    using Bridge.Academies.Api.Translators;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.AspNetCore;
    using Bridge.Framework.AspNetCore.AzureActiveDirectory;
    using Bridge.Framework.AspNetCore.Status;
    using Bridge.Framework.ComponentModel;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using Odachi.AspNetCore.Authentication.Basic;

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddConfigurationServices(
            this IServiceCollection services,
            IConfiguration configuration,
            out AzureActiveDirectoryOptions azureActiveDirectoryOptions)
        {
            services
                .AddSingleton(configuration)
                .Configure<AppSettings>(configuration)
                .Configure<AzureActiveDirectoryOptions>(configuration.GetSection(nameof(AppSettings.AzureActiveDirectory)))
                .Configure<BasicOptions>(
                    BasicDefaults.AuthenticationScheme,
                    configuration.GetSection(nameof(AppSettings.BasicAuthentication)))
                .Configure<DatabaseSettings>(configuration.GetSection(nameof(AppSettings.Database)))
                .Configure<MultiTenancySettings>(configuration.GetSection(nameof(AppSettings.MultiTenancy)));
            azureActiveDirectoryOptions = services
                .BuildServiceProvider()
                .GetRequiredService<IOptions<AzureActiveDirectoryOptions>>()
                .Value;
            return services;
        }

        public static IServiceCollection AddCommandServices(this IServiceCollection services) =>
            services
                .AddScopedLazy<IGetAcademyCommand, GetAcademyCommand>()
                .AddScopedLazy<IGetAcademyFullCommand, GetAcademyFullCommand>()
                .AddScopedLazy<IGetAcademyLocationCommand, GetAcademyLocationCommand>()
                .AddScopedLazy<IGetAllAcademiesCommand, GetAllAcademiesCommand>()
                .AddScopedLazy<IGetAllAcademiesFullCommand, GetAllAcademiesFullCommand>()
                .AddScopedLazy<IGetAllAcademyLocationsCommand, GetAllAcademyLocationsCommand>()
                .AddScopedLazy<IGetAllNearestAcademiesFullCommand, GetAllNearestAcademiesFullCommand>()
                .AddScopedLazy<IGetAllGradesCommand, GetAllGradesCommand>()
                .AddScopedLazy<IGetAllGradesV1Command, GetAllGradesV1Command>()
                .AddScopedLazy<IGetAcademyDailyMetricsCommand, GetAcademyDailyMetricsCommand>()
                .AddScopedLazy<IGetAllClassroomsCommand, GetAllClassroomsCommand>()
                .AddScopedLazy<IGetAcademyClassroomsCommand, GetAcademyClassroomsCommand>()
                .AddScopedLazy<IGetAcademyPtrRankingCommand, GetAcademyPtrRankingCommand>()
                .AddScopedLazy<IGetAcademyProbableNaicsCommand, GetAcademyProbableNaicsCommand>()
                .AddScopedLazy<IGetAcademiesFullCommand, GetAcademiesFullCommand>();

        public static IServiceCollection AddRepositoryServices(this IServiceCollection services) =>
            services
                .AddScoped<IBridgeDbContext, BridgeDbContext>()
                .AddScoped<IWarehouseDbContext, WarehouseDbContext>()
                .AddScoped<IKeyMetricsRepository, KeyMetricsRepository>()
                .AddScoped<IPtrRankingRepository, PtrRankingRepository>()
                .AddScoped<IAcademiesRepository, AcademiesRepository>()
                .AddScoped<IGradesRepository, GradesRepository>()
                .AddScoped<IProbableNaicsRepository, ProbableNaicsRepository>()
                .AddScoped<IClassroomRepository, ClassroomRepository>();

        public static IServiceCollection AddServicesServices(this IServiceCollection services) =>
            services
                .AddScoped<IConnectionTester, ConnectionTesterService>()
                .AddSingleton<IPolicyService, PolicyService>()
                .AddSingleton<ITenantService, TenantService>();

        public static IServiceCollection AddTranslatorServices(this IServiceCollection services) =>
            services
                .AddSingleton<ITranslator<Models.AcademyLocation, Coordinates>, AcademyLocationToCoordinatesTranslator>()
                .AddSingleton<ITranslator<Models.Academy, AcademyFull>, AcademyToAcademyFullTranslator>()
                .AddSingleton<ITranslator<Models.Academy, AcademyLocation>, AcademyToAcademyLocationTranslator>()
                .AddSingleton<ITranslator<Models.Academy, Academy>, AcademyToAcademyTranslator>()
                .AddSingleton<ITranslator<Models.Region, Location>, RegionToLocationTranslator>()
                .AddSingleton<ITranslator<Models.Grades, GradeV1>, GradeToGradeV1Translator>()
                .AddSingleton<ITranslator<Models.Grades, Grade>, GradeToGradeTranslator>()
                .AddSingleton<ITranslator<Models.GradeDailyMetric, AcademyDailyMetrics>, GradeToAcademyDailyMetricsTranslator>()
                .AddSingleton<ITranslator<Models.Classroom, Classroom>, ClassroomToClassroomTranslator>()
                .AddSingleton<ITranslator<Models.PtrRanking, PtrRanking>, PtrRankingToPtrRankingTranslator>()
                .AddSingleton<ITranslator<Models.ProbableNaics, AcademyProbableNaics>, ProbableNaicsToAcademyProbableNaicsTranslator>()
                .AddSingleton<ITranslator<Models.Academy, AcademyAll>, AcademyToAcademyAllTranslator>();
    }
}