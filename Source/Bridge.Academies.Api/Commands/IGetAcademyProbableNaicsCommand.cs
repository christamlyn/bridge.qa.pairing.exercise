namespace Bridge.Academies.Api.Commands
{
    using Bridge.Framework.AspNetCore;

    public interface IGetAcademyProbableNaicsCommand : IAsyncCommand<int?, int>
    {
    }
}
