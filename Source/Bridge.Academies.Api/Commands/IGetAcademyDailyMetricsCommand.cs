namespace Bridge.Academies.Api.Commands
{
    using System;
    using Bridge.Framework.AspNetCore;

    public interface IGetAcademyDailyMetricsCommand : IAsyncCommand<int?, int, DateTimeOffset?>
    {
    }
}
