namespace Bridge.Academies.Api.Commands
{
    using System;
    using Bridge.Framework.AspNetCore;

    public interface IGetAcademyPtrRankingCommand : IAsyncCommand<int?, int>
    {
    }
}
