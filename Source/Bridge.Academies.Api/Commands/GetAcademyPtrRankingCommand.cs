namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAcademyPtrRankingCommand : IGetAcademyPtrRankingCommand
    {
        private readonly IPtrRankingRepository ptrRankingRepository;
        private readonly ITranslator<Models.PtrRanking, PtrRanking> academyPtrRankingTranslator;
        private readonly ITenantService tenantService;

        public GetAcademyPtrRankingCommand(
            IPtrRankingRepository ptrRankingRepository,
            ITranslator<Models.PtrRanking, PtrRanking> academyPtrRankingTranslator,
            ITenantService tenantService)
        {
            this.ptrRankingRepository = ptrRankingRepository;
            this.academyPtrRankingTranslator = academyPtrRankingTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(int? tenantId, int academyId, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var ptrRankingModel = await this.ptrRankingRepository.GetPtrRanking(academyId, cancellationToken);
            if (ptrRankingModel == null)
            {
                return new NotFoundResult();
            }

            var ptrRankingViewModel = this.academyPtrRankingTranslator.Translate(ptrRankingModel);
            return new OkObjectResult(ptrRankingModel);
        }
    }
}
