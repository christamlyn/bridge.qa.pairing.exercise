namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAllGradesCommand : IGetAllGradesCommand
    {
        private readonly IGradesRepository gradesRepository;
        private readonly ITranslator<Models.Grades, Grade> gradeTranslator;
        private readonly ITenantService tenantService;

        public GetAllGradesCommand(
            IGradesRepository gradesRepository,
            ITranslator<Models.Grades, Grade> gradeTranslator,
            ITenantService tenantService)
        {
            this.gradesRepository = gradesRepository;
            this.gradeTranslator = gradeTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(int? tenantId, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var gradeModel = await this.gradesRepository.GetAllGrades(cancellationToken);
            if (gradeModel == null)
            {
                return new NotFoundResult();
            }

            var academyViewModel = this.gradeTranslator.TranslateList(gradeModel);
            return new OkObjectResult(academyViewModel);
        }
    }
}
