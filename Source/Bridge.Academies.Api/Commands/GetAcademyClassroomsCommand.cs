namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAcademyClassroomsCommand : IGetAcademyClassroomsCommand
    {
        private readonly IClassroomRepository classroomRepository;
        private readonly ITranslator<Models.Classroom, Classroom> classroomTranslator;
        private readonly ITenantService tenantService;

        public GetAcademyClassroomsCommand(
            IClassroomRepository classroomRepository,
            ITranslator<Models.Classroom, Classroom> classroomTranslator,
            ITenantService tenantService)
        {
            this.classroomRepository = classroomRepository;
            this.classroomTranslator = classroomTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(int? tenantId, int academyId, AcademyClassroomRequest request, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            return await Task.FromResult<IActionResult>(null);
        }
    }
}
