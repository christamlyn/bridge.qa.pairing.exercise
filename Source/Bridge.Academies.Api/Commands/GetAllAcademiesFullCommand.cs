namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAllAcademiesFullCommand : IGetAllAcademiesFullCommand
    {
        private readonly IAcademiesRepository academiesRepository;
        private readonly ITranslator<Models.Academy, AcademyFull> academyTranslator;
        private readonly ITenantService tenantService;

        public GetAllAcademiesFullCommand(
            IAcademiesRepository academiesRepository,
            ITranslator<Models.Academy, AcademyFull> academyTranslator,
              ITenantService tenantService)
        {
            this.academiesRepository = academiesRepository;
            this.academyTranslator = academyTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(AcademiesRequest request, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(request.TenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var models = await this.academiesRepository.GetAllAcademies(
                request.AcademyId,
                request.IsActive,
                true,
                request.ModifiedSince,
                cancellationToken);
            var viewModels = this.academyTranslator.TranslateList(models);
            return new OkObjectResult(viewModels);
        }
    }
}
