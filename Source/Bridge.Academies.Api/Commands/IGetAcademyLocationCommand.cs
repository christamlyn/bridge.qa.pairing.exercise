namespace Bridge.Academies.Api.Commands
{
    using Bridge.Framework.AspNetCore;

    public interface IGetAcademyLocationCommand : IAsyncCommand<int?, int>
    {
    }
}
