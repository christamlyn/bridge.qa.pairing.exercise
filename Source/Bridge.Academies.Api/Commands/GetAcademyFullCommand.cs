namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAcademyFullCommand : IGetAcademyFullCommand
    {
        private readonly IAcademiesRepository academiesRepository;
        private readonly ITranslator<Models.Academy, AcademyFull> academyTranslator;
        private readonly ITenantService tenantService;

        public GetAcademyFullCommand(
            IAcademiesRepository academiesRepository,
            ITranslator<Models.Academy, AcademyFull> academyTranslator,
            ITenantService tenantService)
        {
            this.academiesRepository = academiesRepository;
            this.academyTranslator = academyTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(int? tenantId, int academyId, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var academyModel = await this.academiesRepository.GetAcademy(academyId, true, cancellationToken);
            if (academyModel == null)
            {
                return new NotFoundResult();
            }

            var academyViewModel = this.academyTranslator.Translate(academyModel);
            return new OkObjectResult(academyViewModel);
        }
    }
}
