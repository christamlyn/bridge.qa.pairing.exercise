namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAllClassroomsCommand : IGetAllClassroomsCommand
    {
        private readonly IClassroomRepository classroomsRepository;
        private readonly ITranslator<Models.Classroom, Classroom> classroomTranslator;
        private readonly ITenantService tenantService;

        public GetAllClassroomsCommand(
            IClassroomRepository classroomsRepository,
            ITranslator<Models.Classroom, Classroom> classroomTranslator,
            ITenantService tenantService)
        {
            this.classroomsRepository = classroomsRepository;
            this.classroomTranslator = classroomTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(int? tenantId, int academyId, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var classroomModel = await this.classroomsRepository.GetAllClassrooms(academyId, cancellationToken);
            if (classroomModel == null)
            {
                return new NotFoundResult();
            }

            var classroomViewModel = this.classroomTranslator.TranslateList(classroomModel);
            return new OkObjectResult(classroomViewModel);
        }
    }
}
