namespace Bridge.Academies.Api.Commands
{
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAllNearestAcademiesFullCommand : IGetAllNearestAcademiesFullCommand
    {
        private readonly IAcademiesRepository academiesRepository;
        private readonly ITranslator<Models.Academy, AcademyFull> academyTranslator;
        private readonly ITenantService tenantService;

        public GetAllNearestAcademiesFullCommand(
            IAcademiesRepository academiesRepository,
            ITranslator<Models.Academy, AcademyFull> academyTranslator,
            ITenantService tenantService)
        {
            this.academiesRepository = academiesRepository;
            this.academyTranslator = academyTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(
            double latitude,
            double longitude,
            NearestAcademiesRequest request,
            CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(request.TenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var academies = await this.academiesRepository.GetAllAcademies(
                null,
                request.IsActive,
                true,
                null,
                cancellationToken);
            var academyViewModels = academies
                .Where(x => x.AcademyLocation != null && x.AcademyLocation.GPSLat.HasValue && x.AcademyLocation.GPSLong.HasValue)
                .Select(x =>
                {
                    var nearestAcademyFull = new NearestAcademyFull();
                    this.academyTranslator.Translate(x, nearestAcademyFull);
                    nearestAcademyFull.Distance = x.AcademyLocation.GetDistanceTo(latitude, longitude);
                    return nearestAcademyFull;
                })
                .OrderBy(x => x.Distance)
                .Skip(request.Skip ?? 0)
                .Take(request.Count ?? academies.Count)
                .ToList();

            return new OkObjectResult(academyViewModels);
        }
    }
}
