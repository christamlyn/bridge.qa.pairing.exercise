namespace Bridge.Academies.Api.Commands
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.AspNetCore;

    public interface IGetAcademyClassroomsCommand : IAsyncCommand<int?, int, AcademyClassroomRequest>
    {
    }
}
