namespace Bridge.Academies.Api.Commands
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAcademyDailyMetricsCommand : IGetAcademyDailyMetricsCommand
    {
        private readonly IKeyMetricsRepository keyMetricsRepository;
        private readonly ITranslator<Models.GradeDailyMetric, AcademyDailyMetrics> academyDailyMetricTranslator;
        private readonly ITenantService tenantService;

        public GetAcademyDailyMetricsCommand(
            IKeyMetricsRepository keyMetricsRepository,
            ITranslator<Models.GradeDailyMetric, AcademyDailyMetrics> academyDailyMetricTranslator,
            ITenantService tenantService)
        {
            this.keyMetricsRepository = keyMetricsRepository;
            this.academyDailyMetricTranslator = academyDailyMetricTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(
            int? tenantId,
            int academyId,
            DateTimeOffset? filterDate,
            CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var dailyMetricsModel = await this.keyMetricsRepository.GetDailyMetrics(
                academyId,
                filterDate,
                cancellationToken);
            if (dailyMetricsModel == null)
            {
                return new NotFoundResult();
            }

            var dailyMetricsViewModel = this.academyDailyMetricTranslator.Translate(dailyMetricsModel);
            return new OkObjectResult(dailyMetricsViewModel);
        }
    }
}
