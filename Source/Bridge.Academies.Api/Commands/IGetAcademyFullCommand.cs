namespace Bridge.Academies.Api.Commands
{
    using Bridge.Framework.AspNetCore;

    public interface IGetAcademyFullCommand : IAsyncCommand<int?, int>
    {
    }
}
