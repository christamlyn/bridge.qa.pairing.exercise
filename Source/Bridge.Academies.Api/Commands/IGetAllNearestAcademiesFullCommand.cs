namespace Bridge.Academies.Api.Commands
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.AspNetCore;

    public interface IGetAllNearestAcademiesFullCommand : IAsyncCommand<double, double, NearestAcademiesRequest>
    {
    }
}
