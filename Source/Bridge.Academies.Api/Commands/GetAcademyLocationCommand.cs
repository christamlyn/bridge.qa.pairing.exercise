namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAcademyLocationCommand : IGetAcademyLocationCommand
    {
        private readonly IAcademiesRepository academiesRepository;
        private readonly ITranslator<Models.Academy, AcademyLocation> academyTranslator;
        private readonly ITenantService tenantService;

        public GetAcademyLocationCommand(
            IAcademiesRepository academiesRepository,
            ITranslator<Models.Academy, AcademyLocation> academyTranslator,
            ITenantService tenantService)
        {
            this.academiesRepository = academiesRepository;
            this.academyTranslator = academyTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(int? tenantId, int academyId, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var model = await this.academiesRepository.GetAcademy(academyId, true, cancellationToken);
            if (model == null)
            {
                return new NotFoundResult();
            }

            var viewModel = this.academyTranslator.Translate(model);
            return new OkObjectResult(viewModel);
        }
    }
}
