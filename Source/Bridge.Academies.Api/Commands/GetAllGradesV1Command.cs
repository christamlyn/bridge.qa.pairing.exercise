namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAllGradesV1Command : IGetAllGradesV1Command
    {
        private readonly IGradesRepository gradesRepository;
        private readonly ITranslator<Models.Grades, GradeV1> gradeTranslator;

        public GetAllGradesV1Command(
            IGradesRepository gradesRepository,
            ITranslator<Models.Grades, GradeV1> gradeTranslator)
        {
            this.gradesRepository = gradesRepository;
            this.gradeTranslator = gradeTranslator;
        }

        public async Task<IActionResult> ExecuteAsync(CancellationToken cancellationToken)
        {
            var gradeModel = await this.gradesRepository.GetAllGrades(cancellationToken);
            if (gradeModel == null)
            {
                return new NotFoundResult();
            }

            var academyViewModel = this.gradeTranslator.TranslateList(gradeModel);
            return new OkObjectResult(academyViewModel);
        }
    }
}
