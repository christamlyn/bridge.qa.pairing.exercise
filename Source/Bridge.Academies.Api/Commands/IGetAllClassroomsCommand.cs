namespace Bridge.Academies.Api.Commands
{
    using Bridge.Framework.AspNetCore;

    public interface IGetAllClassroomsCommand : IAsyncCommand<int?, int>
    {
    }
}
