namespace Bridge.Academies.Api.Commands
{
    using System.Threading;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Repositories;
    using Bridge.Academies.Api.Services;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.ComponentModel;
    using Microsoft.AspNetCore.Mvc;

    public class GetAcademyProbableNaicsCommand : IGetAcademyProbableNaicsCommand
    {
        private readonly IProbableNaicsRepository probableNaicsRepository;
        private readonly ITranslator<Models.ProbableNaics, AcademyProbableNaics> academyProbableNaicsTranslator;
        private readonly ITenantService tenantService;

        public GetAcademyProbableNaicsCommand(
            IProbableNaicsRepository probableNaicsRepository,
            ITranslator<Models.ProbableNaics, AcademyProbableNaics> academyProbableNaicsTranslator,
            ITenantService tenantService)
        {
            this.probableNaicsRepository = probableNaicsRepository;
            this.academyProbableNaicsTranslator = academyProbableNaicsTranslator;
            this.tenantService = tenantService;
        }

        public async Task<IActionResult> ExecuteAsync(int? tenantId, int academyId, CancellationToken cancellationToken)
        {
            var modelState = this.tenantService.ValidateTenant(tenantId);
            if (!modelState.IsValid)
            {
                return new BadRequestObjectResult(modelState);
            }

            var probableNaicsModel = await this.probableNaicsRepository.GetProbableNaics(academyId, cancellationToken);
            if (probableNaicsModel == null)
            {
                return new NotFoundResult();
            }

            var probableNaicsViewModel = this.academyProbableNaicsTranslator.Translate(probableNaicsModel);
            return new OkObjectResult(probableNaicsViewModel);
        }
    }
}
