namespace Bridge.Academies.Api.Commands
{
    using Bridge.Framework.AspNetCore;

    public interface IGetAllGradesCommand : IAsyncCommand<int?>
    {
    }
}
