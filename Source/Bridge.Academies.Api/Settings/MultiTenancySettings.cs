namespace Bridge.Academies.Api.Settings
{
    using System.Collections.Generic;

    public class MultiTenancySettings
    {
        public List<TenantSettings> Tenants { get; set; }
    }
}
