namespace Bridge.Academies.Api.Settings
{
    public class TenantDatabaseSettings
    {
        public string Server { get; set; }

        public string Catalog { get; set; }

        public string Key { get; set; }
    }
}
