namespace Bridge.Academies.Api.Settings
{
    using Bridge.Framework.AspNetCore.AzureActiveDirectory;
    using Odachi.AspNetCore.Authentication.Basic;

    public class AppSettings
    {
        public AzureActiveDirectoryOptions AzureActiveDirectory { get; set; }

        public BasicOptions BasicAuthentication { get; set; }

        public DatabaseSettings Database { get; set; }

        public MultiTenancySettings MultiTenancy { get; set; }
    }
}
