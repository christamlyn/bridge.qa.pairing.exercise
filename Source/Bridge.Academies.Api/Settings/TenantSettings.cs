namespace Bridge.Academies.Api.Settings
{
    using System.Collections.Generic;

    public class TenantSettings
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<TenantDatabaseSettings> Databases { get; set; }

        public bool? HealthCheck { get; set; }
    }
}
