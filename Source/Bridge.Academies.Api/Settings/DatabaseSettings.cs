﻿namespace Bridge.Academies.Api.Settings
{
    using Bridge.Framework.ExceptionHandling;

    public class DatabaseSettings
    {
        public string BDMConnectionString { get; set; }

        public string BridgeConnectionString { get; set; }

        public PolicyOptions ExceptionHandling { get; set; }
    }
}