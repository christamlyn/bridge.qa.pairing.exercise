﻿namespace Bridge.Academies.Api.Constants
{
    public static class AuthorizationPolicyName
    {
        public const string AcademyRead = nameof(AcademyRead);
        public const string ClassroomRead = nameof(ClassroomRead);
        public const string GradeRead = nameof(GradeRead);
    }
}
