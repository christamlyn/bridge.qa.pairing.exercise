namespace Bridge.Academies.Api.Constants
{
    public static class ClassroomsControllerRouteName
    {
        public const string GetAllClassrooms = ControllerName.Classrooms + nameof(GetAllClassrooms);
        public const string GetAcademyClassrooms = ControllerName.Classrooms + nameof(GetAcademyClassrooms);
    }
}
