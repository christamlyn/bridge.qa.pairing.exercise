﻿namespace Bridge.Academies.Api.Constants
{
    public static class GradesControllerV1RoutesName
    {
        public const string GetAllGrades = ControllerName.GradesV1 + nameof(GetAllGrades);
    }
}
