﻿namespace Bridge.Academies.Api.Constants
{
    public static class GradesControllerRoutesName
    {
        public const string GetAllGrades = ControllerName.Grades + nameof(GetAllGrades);
    }
}
