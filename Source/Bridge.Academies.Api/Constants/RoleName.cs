﻿namespace Bridge.Academies.Api.Constants
{
    public static class RoleName
    {
        public const string AcademyReadAll = "Academy.Read.All";
        public const string ClassroomReadAll = "Classroom.Read.All";
        public const string GradeReadAll = "Grade.Read.All";
    }
}
