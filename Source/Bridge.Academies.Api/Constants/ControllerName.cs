﻿namespace Bridge.Academies.Api.Constants
{
    public static class ControllerName
    {
        public const string Academies = nameof(Academies);
        public const string Home = nameof(Home);
        public const string Status = nameof(Status);
        public const string Grades = nameof(Grades);
        public const string GradesV1 = nameof(GradesV1);
        public const string Classrooms = nameof(Classrooms);
    }
}
