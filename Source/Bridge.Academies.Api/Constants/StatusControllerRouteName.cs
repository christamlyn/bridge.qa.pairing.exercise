﻿namespace Bridge.Academies.Api.Constants
{
    public static class StatusControllerRouteName
    {
        public const string GetStatus = ControllerName.Status + nameof(GetStatus);
    }
}
