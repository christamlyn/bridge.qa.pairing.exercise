﻿namespace Bridge.Academies.Api.Constants
{
    public static class HomeControllerRouteName
    {
        public const string GetHome = ControllerName.Home + nameof(GetHome);
    }
}
