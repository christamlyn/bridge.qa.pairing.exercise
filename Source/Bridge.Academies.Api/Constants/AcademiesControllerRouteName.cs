namespace Bridge.Academies.Api.Constants
{
    public static class AcademiesControllerRouteName
    {
        public const string Get = ControllerName.Academies + nameof(Get);
        public const string GetFull = ControllerName.Academies + nameof(GetFull);
        public const string GetLocation = ControllerName.Academies + nameof(GetLocation);
        public const string GetAll = ControllerName.Academies + nameof(GetAll);
        public const string GetAllFull = ControllerName.Academies + nameof(GetAllFull);
        public const string GetAllLocation = ControllerName.Academies + nameof(GetAllLocation);
        public const string GetAllNearestFull = ControllerName.Academies + nameof(GetAllNearestFull);
        public const string GetDailyMetrics = ControllerName.Academies + nameof(GetDailyMetrics);
        public const string GetPtrRanking = ControllerName.Academies + nameof(GetPtrRanking);
        public const string AcademyUser = ControllerName.Academies + nameof(AcademyUser);
        public const string GetProbableNaics = ControllerName.Academies + nameof(GetProbableNaics);
        public const string GetAcademiesFull = ControllerName.Academies + nameof(GetAcademiesFull);
    }
}
