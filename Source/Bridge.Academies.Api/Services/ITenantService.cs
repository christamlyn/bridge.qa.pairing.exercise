namespace Bridge.Academies.Api.Services
{
    using System.Collections.Generic;
    using Microsoft.AspNetCore.Mvc.ModelBinding;

    public interface ITenantService
    {
        List<string> GetAllHealthCheckConnectionStrings();

        string GetBridgeConnectionString();

        string GetWarehouseConnectionString();

        ModelStateDictionary ValidateTenant(int? tenantId);
    }
}