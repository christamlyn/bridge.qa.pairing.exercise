namespace Bridge.Academies.Api.Services
{
    using Polly;

    public interface IPolicyService
    {
        Policy GetDatabasePolicy();
    }
}
