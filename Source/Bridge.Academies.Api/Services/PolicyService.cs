namespace Bridge.Academies.Api.Services
{
    using System;
    using Bridge.Academies.Api.Settings;
    using Bridge.Framework.ExceptionHandling;
    using Microsoft.Extensions.Options;
    using Polly;

    public class PolicyService : IPolicyService
    {
        private readonly IOptions<AppSettings> appSettings;
        private Policy academiesPolicy;

        public PolicyService(IOptions<AppSettings> appSettings) =>
            this.appSettings = appSettings;

        public Policy GetDatabasePolicy()
        {
            if (this.academiesPolicy == null)
            {
                this.academiesPolicy = Policy
                    .Handle<Exception>()
                    .FromOptionsAsync(this.appSettings.Value.Database.ExceptionHandling);
            }

            return this.academiesPolicy;
        }
    }
}
