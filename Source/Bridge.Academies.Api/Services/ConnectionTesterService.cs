namespace Bridge.Academies.Api.Services
{
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Bridge.Framework.AspNetCore.Status;

    public class ConnectionTesterService : IConnectionTester
    {
        private readonly ITenantService tenantService;

        public ConnectionTesterService(ITenantService tenantService) =>
            this.tenantService = tenantService;

        public async Task TestConnection()
        {
            var connectionStrings = this.tenantService.GetAllHealthCheckConnectionStrings();
            if (connectionStrings != null && connectionStrings.Count > 0)
            {
                var tasks = connectionStrings.Select(x => TestConnection(x));
                await Task.WhenAll(tasks);
            }
        }

        private static async Task TestConnection(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();
            }
        }
    }
}