namespace Bridge.Academies.Api.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Settings;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.AspNetCore.Mvc.ModelBinding;
    using Microsoft.Extensions.Options;
    using Microsoft.Extensions.Primitives;

    public class TenantService : ITenantService
    {
        private const string TenantIdQueryParameter = "tenantId";
        private const string TenantIdHttpHeader = "X-Tenant-ID";
        private const string BridgeDatabaseKey = "Bridge";
        private const string WarehouseDatabaseKey = "Warehouse";
        private readonly DatabaseSettings databaseSettings;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly MultiTenancySettings multiTenancySettings;
        private readonly IActionContextAccessor actionContextAccessor;

        public TenantService(
            IOptions<DatabaseSettings> databaseSettings,
            IHttpContextAccessor httpContextAccessor,
            IOptions<MultiTenancySettings> multiTenancySettings,
            IActionContextAccessor actionContextAccessor)
        {
            this.databaseSettings = databaseSettings.Value;
            this.httpContextAccessor = httpContextAccessor;
            this.multiTenancySettings = multiTenancySettings.Value;
            this.actionContextAccessor = actionContextAccessor;
        }

        public List<string> GetAllHealthCheckConnectionStrings()
        {
            var tenantIds = this.multiTenancySettings.Tenants.Where(x => x.HealthCheck == true)?.Select(x => x.Id).ToList();
            if (tenantIds != null && tenantIds.Count > 0)
            {
                return tenantIds
               .Select(x => this.GetConnectionString(x, this.databaseSettings.BridgeConnectionString, BridgeDatabaseKey))
               .Union(tenantIds.Select(x => this.GetConnectionString(x, this.databaseSettings.BDMConnectionString, WarehouseDatabaseKey)))
               .ToList();
            }

            return new List<string>();
        }

        public string GetBridgeConnectionString() =>
            this.GetConnectionString(
                this.GetCurrentTenantId(),
                this.databaseSettings.BridgeConnectionString,
                BridgeDatabaseKey);

        public string GetWarehouseConnectionString() =>
            this.GetConnectionString(
                this.GetCurrentTenantId(),
                this.databaseSettings.BDMConnectionString,
                WarehouseDatabaseKey);

        public ModelStateDictionary ValidateTenant(int? tenantId)
        {
            var modelState = this.actionContextAccessor.ActionContext.ModelState;
            this.AddModelErrors(tenantId, this.multiTenancySettings, modelState);
            return modelState;
        }

        private void AddModelErrors(int? tenantId, MultiTenancySettings multiTenancySettings, ModelStateDictionary modelState)
        {
            if (!tenantId.HasValue)
            {
                modelState.AddModelError(
                 nameof(tenantId), $"TenantId is not specified");
            }

            if (multiTenancySettings.Tenants.Where(x => x.Id == tenantId).FirstOrDefault() == null)
            {
                modelState.AddModelError(
                  nameof(tenantId), $"Tenant with ID {tenantId} could not be found.");
            }
        }

        private int GetCurrentTenantId()
        {
            var request = this.httpContextAccessor.HttpContext?.Request;
            var query = request?.Query.ToList();
            var headers = request?.Headers?.ToList();
            if (query != null || headers != null)
            {
                var values = query?
                    .Where(x => string.Equals(x.Key, TenantIdQueryParameter, StringComparison.OrdinalIgnoreCase))
                    .Union(headers.Where(x => string.Equals(x.Key, TenantIdHttpHeader, StringComparison.OrdinalIgnoreCase)))
                    .Select(x => x.Value.ToString());
                var value = values.Where(x => !string.IsNullOrWhiteSpace(x)).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(value) &&
                    value != default(string) &&
                    int.TryParse(value, out int tenantId))
                {
                    return tenantId;
                }
            }

            throw new InvalidOperationException("No tenant id in query string");
        }

        private string GetConnectionString(int tenantId, string connectionString, string databaseKey)
        {
            var tenantSettings = this.multiTenancySettings.Tenants.FirstOrDefault(x => x.Id == tenantId);
            if (tenantSettings == null)
            {
                throw new InvalidOperationException("Invalid tenant id");
            }

            var databaseSettings = tenantSettings
                .Databases
                .First(x => string.Equals(x.Key, databaseKey, StringComparison.OrdinalIgnoreCase));
            return new SqlConnectionStringBuilder()
            {
                ConnectionString = connectionString,
                DataSource = databaseSettings.Server,
                InitialCatalog = databaseSettings.Catalog
            }.ToString();
        }
    }
}
