namespace Bridge.Academies.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Constants;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.AspNetCore.Filters;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [ApiVersion("2.0")]
    [Authorize(Policy = AuthorizationPolicyName.GradeRead)]
    [Route("v{version:apiVersion}/[controller]")]
    [RequestIdHttpHeader(Forward = true, Required = true)]
    [UserAgentHttpHeader(Forward = true, Required = true)]
    public class GradesController : ControllerBase
    {
        private readonly Lazy<IGetAllGradesCommand> getAllGradesCommand;

        public GradesController(Lazy<IGetAllGradesCommand> getAllGradesCommand) =>
            this.getAllGradesCommand = getAllGradesCommand;

        /// <summary>
        /// Gets a collection of all grades.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <returns>Returns a 200 OK response containing all grades.</returns>
        /// <response code="200">Returns all grades.</response>
        [HttpGet("", Name = GradesControllerRoutesName.GetAllGrades)]
        [ProducesResponseType(typeof(IEnumerable<Grade>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAllGrades([FromQuery] int? tenantId) =>
            this.getAllGradesCommand.Value.ExecuteAsync(tenantId);
    }
}
