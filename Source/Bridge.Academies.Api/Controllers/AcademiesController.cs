namespace Bridge.Academies.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Constants;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.AspNetCore.Filters;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Perform actions on academies.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Authorize(Policy = AuthorizationPolicyName.AcademyRead)]
    [Route("academies")]
    [RequestIdHttpHeader(Forward = true, Required = true)]
    [UserAgentHttpHeader(Forward = true, Required = true)]
    public class AcademiesController : ControllerBase
    {
        private readonly Lazy<IGetAcademyCommand> getAcademyCommand;
        private readonly Lazy<IGetAcademyFullCommand> getAcademyFullCommand;
        private readonly Lazy<IGetAcademyLocationCommand> getAcademyLocationCommand;
        private readonly Lazy<IGetAllAcademiesCommand> getAllAcademiesCommand;
        private readonly Lazy<IGetAllAcademiesFullCommand> getAllAcademiesFullCommand;
        private readonly Lazy<IGetAllAcademyLocationsCommand> getAllAcademyLocationsCommand;
        private readonly Lazy<IGetAllNearestAcademiesFullCommand> getAllNearestAcademiesFullCommand;
        private readonly Lazy<IGetAcademyDailyMetricsCommand> getAcademyDailyMetricsCommand;
        private readonly Lazy<IGetAcademyPtrRankingCommand> getAcademiesPtrRankingCommand;
        private readonly Lazy<IGetAcademyProbableNaicsCommand> getAcademyProbableNaicsCommand;
        private readonly Lazy<IGetAcademiesFullCommand> getAcademiesFullCommand;

        public AcademiesController(
            Lazy<IGetAcademyCommand> getAcademyCommand,
            Lazy<IGetAcademyFullCommand> getAcademyFullCommand,
            Lazy<IGetAcademyLocationCommand> getAcademyLocationCommand,
            Lazy<IGetAllAcademiesCommand> getAllAcademiesCommand,
            Lazy<IGetAllAcademiesFullCommand> getAllAcademiesFullCommand,
            Lazy<IGetAllAcademyLocationsCommand> getAllAcademyLocationsCommand,
            Lazy<IGetAllNearestAcademiesFullCommand> getAllNearestAcademiesFullCommand,
            Lazy<IGetAcademyDailyMetricsCommand> getAcademyDailyMetricsCommand,
            Lazy<IGetAcademyPtrRankingCommand> getAcademiesPtrRankingCommand,
            Lazy<IGetAcademyProbableNaicsCommand> getAcademyProbableNaicsCommand,
            Lazy<IGetAcademiesFullCommand> getAcademiesFullCommand)
        {
            this.getAcademyCommand = getAcademyCommand;
            this.getAcademyFullCommand = getAcademyFullCommand;
            this.getAcademyLocationCommand = getAcademyLocationCommand;
            this.getAllAcademiesCommand = getAllAcademiesCommand;
            this.getAllAcademiesFullCommand = getAllAcademiesFullCommand;
            this.getAllAcademyLocationsCommand = getAllAcademyLocationsCommand;
            this.getAllNearestAcademiesFullCommand = getAllNearestAcademiesFullCommand;
            this.getAcademyDailyMetricsCommand = getAcademyDailyMetricsCommand;
            this.getAcademiesPtrRankingCommand = getAcademiesPtrRankingCommand;
            this.getAcademyProbableNaicsCommand = getAcademyProbableNaicsCommand;
            this.getAcademiesFullCommand = getAcademiesFullCommand;
        }

        /// <summary>
        /// Gets the basic information for the academy with the specified identifier.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <param name="academyId">The academy identifier.</param>
        /// <returns>The academy or 404 Not Found.</returns>
        /// <response code="200">Returns the Academy with the specified Academy ID, including its basic information.</response>
        /// <response code="404">An Academy with the specified Academy ID was not found.</response>
        [HttpGet("{academyId:int:min(1)}", Name = AcademiesControllerRouteName.Get)]
        [ProducesResponseType(typeof(Academy), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public Task<IActionResult> Get([FromQuery] int? tenantId, int academyId) =>
            this.getAcademyCommand.Value.ExecuteAsync(tenantId, academyId);

        /// <summary>
        /// Gets the full information for the academy with the specified identifier.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <param name="academyId">The academy identifier.</param>
        /// <returns>The academy or 404 Not Found.</returns>
        /// <response code="200">Returns the Academy with the specified Academy ID, including its full information.</response>
        /// <response code="404">An Academy with the specified Academy ID was not found.</response>
        [HttpGet("{academyId:int:min(1)}/full", Name = AcademiesControllerRouteName.GetFull)]
        [ProducesResponseType(typeof(AcademyFull), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetFull([FromQuery] int? tenantId, int academyId) =>
            this.getAcademyFullCommand.Value.ExecuteAsync(tenantId, academyId);

        /// <summary>
        /// Gets the location of the academy with the specified identifier.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <param name="academyId">The academy identifier.</param>
        /// <returns>The academy or 404 Not Found.</returns>
        /// <response code="200">Returns the Academy location with the specified Academy ID.</response>
        /// <response code="404">An Academy with the specified Academy ID was not found.</response>
        [HttpGet("{academyId:int:min(1)}/location", Name = AcademiesControllerRouteName.GetLocation)]
        [ProducesResponseType(typeof(AcademyLocation), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetLocation([FromQuery] int? tenantId, int academyId) =>
            this.getAcademyLocationCommand.Value.ExecuteAsync(tenantId, academyId);

        /// <summary>
        /// Gets the daily metrics of the academy with the specified identifier.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <param name="academyId">The academy identifier.</param>
        /// <param name="filterDate">The filter date.</param>
        /// <returns>The academy daily metrics or 404 Not Found.</returns>
        /// <response code="200">Returns the Academy daily metrics with the specified Academy ID.</response>
        /// <response code="404">An Academy with the specified Academy ID was not found.</response>
        [HttpGet("{academyId:int:min(1)}/dailymetrics", Name = AcademiesControllerRouteName.GetDailyMetrics)]
        [ProducesResponseType(typeof(AcademyDailyMetrics), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetDailyMetrics([FromQuery] int? tenantId, int academyId, [FromQuery] DateTimeOffset? filterDate = null) =>
            this.getAcademyDailyMetricsCommand.Value.ExecuteAsync(tenantId, academyId, filterDate);

        /// <summary>
        /// Gets the Ptr ranking of the academy with the specified identifier.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <param name="academyId">The academy identifier.</param>
        /// <returns>The country's academies Ptr Ranking or 404 Not Found.</returns>
        /// <response code="200">Returns the country's academies Ptr Ranking with the specified Country.</response>
        /// <response code="404">Country's academies Ptr Ranking with the specified Country was not found.</response>
        [HttpGet("{academyId:int:min(1)}/ptrRanking", Name = AcademiesControllerRouteName.GetPtrRanking)]
        [ProducesResponseType(typeof(AcademyDailyMetrics), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetPtrRanking([FromQuery] int? tenantId, int academyId) =>
            this.getAcademiesPtrRankingCommand.Value.ExecuteAsync(tenantId, academyId);

        /// <summary>
        /// Gets a collection of academies containing basic information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The collection of academies.</returns>
        /// <response code="200">Returns all academies with their basic information.</response>
        [HttpGet("", Name = AcademiesControllerRouteName.GetAll)]
        [ProducesResponseType(typeof(IEnumerable<Academy>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAll([FromQuery] AcademiesRequest request) =>
            this.getAllAcademiesCommand.Value.ExecuteAsync(request);

        /// <summary>
        /// Gets a collection of academies containing full information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The collection of academies.</returns>
        /// <response code="200">Returns all academies with their full information.</response>
        [HttpGet("full", Name = AcademiesControllerRouteName.GetAllFull)]
        [ProducesResponseType(typeof(IEnumerable<AcademyFull>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAllFull([FromQuery] AcademiesRequest request) =>
            this.getAllAcademiesFullCommand.Value.ExecuteAsync(request);

        /// <summary>
        /// Gets a collection of academies and their locations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The collection of academies.</returns>
        /// <response code="200">Returns all academy locations.</response>
        [HttpGet("location", Name = AcademiesControllerRouteName.GetAllLocation)]
        [ProducesResponseType(typeof(IEnumerable<AcademyLocation>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAllLocation([FromQuery] AcademiesRequest request) =>
            this.getAllAcademyLocationsCommand.Value.ExecuteAsync(request);

        /// <summary>
        /// Gets a collection of the nearest academies containing full information.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="request">The request.</param>
        /// <returns>The collection of academies.</returns>
        /// <response code="200">Returns all academies with their full information.</response>
        [AllowAnonymous]
        [HttpGet("full/nearest/{latitude}/{longitude}", Name = AcademiesControllerRouteName.GetAllNearestFull)]
        [ProducesResponseType(typeof(IEnumerable<NearestAcademyFull>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAllNearestFull(double latitude, double longitude, [FromQuery] NearestAcademiesRequest request) =>
            this.getAllNearestAcademiesFullCommand.Value.ExecuteAsync(latitude, longitude, request);

        /// <summary>
        /// Gets the daily metrics of the academy with the specified identifier.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <param name="academyId">The academy identifier.</param>
        /// <returns>The academy daily metrics or 404 Not Found.</returns>
        /// <response code="200">Returns the Academy daily metrics with the specified Academy ID.</response>
        /// <response code="404">An Academy with the specified Academy ID was not found.</response>
        [HttpGet("{academyId}/probableNaics", Name = AcademiesControllerRouteName.GetProbableNaics)]
        [ProducesResponseType(typeof(AcademyProbableNaics), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status404NotFound)]
        public Task<IActionResult> GetProbableNaics([FromQuery] int? tenantId, int academyId) =>
            this.getAcademyProbableNaicsCommand.Value.ExecuteAsync(tenantId, academyId);

        /// <summary>
        /// Gets a collection of academies containing full information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The collection of academies.</returns>
        /// <response code="200">Returns all academies with their full information.</response>
        [HttpGet("academiesfull", Name = AcademiesControllerRouteName.GetAcademiesFull)]
        [ProducesResponseType(typeof(IEnumerable<AcademyAll>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAcademiesFull([FromQuery] AcademiesRequest request) =>
            this.getAcademiesFullCommand.Value.ExecuteAsync(request);
    }
}