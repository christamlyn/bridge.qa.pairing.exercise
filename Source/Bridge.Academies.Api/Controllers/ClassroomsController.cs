namespace Bridge.Academies.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Constants;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.AspNetCore.Filters;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Perform actions on classrooms.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Authorize(Policy = AuthorizationPolicyName.ClassroomRead)]
    [Route("classrooms")]
    [RequestIdHttpHeader(Forward = true, Required = true)]
    [UserAgentHttpHeader(Forward = true, Required = true)]
    public class ClassroomsController : ControllerBase
    {
        private readonly Lazy<IGetAllClassroomsCommand> getAllClassroomsCommand;
        private readonly Lazy<IGetAcademyClassroomsCommand> getAcademyClassroomsCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClassroomsController"/> class.
        /// </summary>
        /// <param name="getAllClassroomsCommand">The get classroom command.</param>
        /// <param name="getAcademyClassroomsCommand">Get academy classroom request</param>
        public ClassroomsController(
            Lazy<IGetAllClassroomsCommand> getAllClassroomsCommand,
            Lazy<IGetAcademyClassroomsCommand> getAcademyClassroomsCommand)
        {
            this.getAllClassroomsCommand = getAllClassroomsCommand;
            this.getAcademyClassroomsCommand = getAcademyClassroomsCommand;
        }

        /// <summary>
        /// Gets a collection of all classrooms in the specified academy.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <param name="academyId">The academy identifier.</param>
        /// <returns>
        /// A collection of classrooms.
        /// </returns>
        /// <response code="200">Returns all classrooms from the specified academy.</response>
        [HttpGet("{academyId:int:min(1)}", Name = ClassroomsControllerRouteName.GetAllClassrooms)]
        [ProducesResponseType(typeof(IEnumerable<Classroom>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAllClassrooms([FromQuery] int? tenantId, int academyId) =>
            this.getAllClassroomsCommand.Value.ExecuteAsync(tenantId, academyId);

        [HttpGet("{academyId:int:min(1)}", Name = ClassroomsControllerRouteName.GetAcademyClassrooms)]
        [ProducesResponseType(typeof(IEnumerable<Classroom>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAcademyClassrooms([FromQuery] int? tenantId, int academyId, AcademyClassroomRequest request) =>
            this.getAcademyClassroomsCommand.Value.ExecuteAsync(tenantId, academyId, request);
    }
}
