namespace Bridge.Academies.Api.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Bridge.Academies.Api.Commands;
    using Bridge.Academies.Api.Constants;
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework.AspNetCore.Filters;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [ApiVersion("1.0", Deprecated = true)]
    [Authorize(Policy = AuthorizationPolicyName.GradeRead)]
    [Route("grades")]
    [RequestIdHttpHeader(Forward = true, Required = true)]
    [UserAgentHttpHeader(Forward = true, Required = true)]
    public class GradesV1Controller : ControllerBase
    {
        private readonly Lazy<IGetAllGradesV1Command> getAllGradesV1Command;

        /// <summary>
        /// Initializes a new instance of the <see cref="GradesV1Controller"/> class.
        /// </summary>
        /// <param name="getAllGradesV1Command">The get classroom version one command.</param>
        public GradesV1Controller(Lazy<IGetAllGradesV1Command> getAllGradesV1Command)
        {
            this.getAllGradesV1Command = getAllGradesV1Command;
        }

        /// <summary>
        /// Gets a collection of all grades.
        /// </summary>
        /// <param name="tenantId">The tenant unique identifier.</param>
        /// <returns>Returns a 200 OK response containing all grades.</returns>
        /// <response code="200">Returns all grades.</response>
        [HttpGet("", Name = GradesControllerV1RoutesName.GetAllGrades)]
        [ProducesResponseType(typeof(IEnumerable<GradeV1>), StatusCodes.Status200OK)]
        public Task<IActionResult> GetAllGrades([FromQuery] int? tenantId) =>
            this.getAllGradesV1Command.Value.ExecuteAsync();
    }
}
