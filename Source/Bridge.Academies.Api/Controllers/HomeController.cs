﻿namespace Bridge.Academies.Api.Controllers
{
    using Bridge.Academies.Api.Constants;
    using Microsoft.AspNetCore.Mvc;

    [Route("/")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        [HttpGet("/", Name = HomeControllerRouteName.GetHome)]
        public IActionResult Home()
        {
            return this.RedirectPermanent("/swagger");
        }
    }
}
