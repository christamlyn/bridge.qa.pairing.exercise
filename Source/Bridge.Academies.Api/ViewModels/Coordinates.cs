﻿namespace Bridge.Academies.Api.ViewModels
{
    public class Coordinates
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double CommunityRadius { get; set; }
    }
}
