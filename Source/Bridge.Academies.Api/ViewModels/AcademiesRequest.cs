namespace Bridge.Academies.Api.ViewModels
{
    using System;
    using System.Collections.Generic;

    public class AcademiesRequest
    {
        public List<int> AcademyId { get; set; }

        [Obsolete("Use AcademyId instead")]
        public List<int> AcademyIds
        {
            get { return this.AcademyId; }
            set { this.AcademyId = value; }
        }

        public bool? IsActive { get; set; }

        public DateTimeOffset? ModifiedSince { get; set; }

        public int? TenantId { get; set; }
    }
}
