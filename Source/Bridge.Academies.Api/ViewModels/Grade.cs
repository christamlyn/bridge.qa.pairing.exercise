﻿namespace Bridge.Academies.Api.ViewModels
{
    using Bridge.Academies.Api.ViewModelSchemaFilters;
    using Swashbuckle.AspNetCore.SwaggerGen;

    [SwaggerSchemaFilter(typeof(GradeSchemaFilter))]
    public class Grade
    {
        public string Code { get; set; }

        public int GradeId { get; set; }

        public string Name { get; set; }

        public int Order { get; set; }

        public int? MaxAgeOffset { get; set; }

        public int? MinAgeOffset { get; set; }
    }
}
