﻿namespace Bridge.Academies.Api.ViewModels
{
    using System;

    public class Classroom
    {
        public string ClassroomName { get; set; }

        public bool Active { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? DateInactivated { get; set; }
    }
}