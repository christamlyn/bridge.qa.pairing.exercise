﻿namespace Bridge.Academies.Api.ViewModels
{
    using System;
    using Newtonsoft.Json;

    public class AcademyDailyMetrics
    {
        public DateTimeOffset Date { get; set; }

        public int ActiveCount { get; set; }

        public int ActivePTR { get; set; }

        public int AicCount { get; set; }

        public int AicPTR { get; set; }

        public int NaicCount { get; set; }

        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTimeOffset LastUpdatedDate { get; set; }
    }
}
