namespace Bridge.Academies.Api.ViewModels
{
    public class NearestAcademiesRequest
    {
        public int? Count { get; set; }

        public bool? IsActive { get; set; }

        public int? Skip { get; set; }

        public int? TenantId { get; set; }
    }
}
