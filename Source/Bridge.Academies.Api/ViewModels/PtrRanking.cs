﻿namespace Bridge.Academies.Api.ViewModels
{
    using System;
    using Newtonsoft.Json;

    public class PtrRanking
    {
        public long AcademyId { get; set; }

        // Denotes academy rank based on AicPTR
        public int Ranking { get; set; }

        // Denotes total number of academies
        public int TotalAcademies { get; set; }

        public int AicPTR { get; set; }
    }
}
