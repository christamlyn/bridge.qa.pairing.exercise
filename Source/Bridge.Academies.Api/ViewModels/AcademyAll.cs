namespace Bridge.Academies.Api.ViewModels
{
    using System;

    public class AcademyAll
    {
        public int AcademyId { get; set; }

        public string AcademyManager { get; set; }

        public string ShortCode { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        public bool Active { get; set; }

        public DateTimeOffset LaunchDate { get; set; }

        public int? RegionId { get; set; }

        public Location Location { get; set; }
    }
}
