﻿namespace Bridge.Academies.Api.ViewModels
{
    using Bridge.Academies.Api.ViewModelSchemaFilters;
    using Newtonsoft.Json;
    using Swashbuckle.AspNetCore.SwaggerGen;

    [SwaggerSchemaFilter(typeof(AcademyFullSchemaFilter))]
    public class AcademyFull : Academy
    {
        [JsonProperty(Order = 100)]
        public Location Location { get; set; }
    }
}
