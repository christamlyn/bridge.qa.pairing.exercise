namespace Bridge.Academies.Api.ViewModels
{
    using System;

    public class AcademyClassroomRequest
    {
        public int? GradeId { get; set; }

        public DateTime ActiveOn { get; set; }

        public int? TenantId { get; set; }

        public DateTimeOffset? ModifiedSince { get; set; }
    }
}
