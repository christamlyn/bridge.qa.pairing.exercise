namespace Bridge.Academies.Api.ViewModels
{
    using Bridge.Academies.Api.ViewModelSchemaFilters;
    using Swashbuckle.AspNetCore.SwaggerGen;

    [SwaggerSchemaFilter(typeof(NearestAcademyFullSchemaFilter))]
    public class NearestAcademyFull : AcademyFull
    {
        public double Distance { get; set; }
    }
}
