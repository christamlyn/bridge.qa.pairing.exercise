﻿namespace Bridge.Academies.Api.ViewModels
{
    public class Location
    {
        public Coordinates Coordinates { get; set; }

        public string Country { get; set; }

        public string Level1 { get; set; }

        public string Level2 { get; set; }

        public string Level3 { get; set; }

        public string Level4 { get; set; }

        public string Level5 { get; set; }

        public string Level6 { get; set; }

        public string Level7 { get; set; }
    }
}