﻿namespace Bridge.Academies.Api.ViewModels
{
    using Bridge.Academies.Api.ViewModelSchemaFilters;
    using Swashbuckle.AspNetCore.SwaggerGen;

    [SwaggerSchemaFilter(typeof(AcademyLocationSchemaFilter))]
    public class AcademyLocation : Location
    {
        public int AcademyId { get; set; }
    }
}
