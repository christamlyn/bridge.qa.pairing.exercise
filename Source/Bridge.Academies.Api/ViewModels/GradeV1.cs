﻿namespace Bridge.Academies.Api.ViewModels
{
    public class GradeV1
    {
        public int GradeId { get; set; }

        public string GradeName { get; set; }

        public string GradeAbbreviation { get; set; }

        public int MaxPupils { get; set; }

        public bool HasPlacement { get; set; }

        public int MinPlacementScore { get; set; }

        public int MaxPlacementScore { get; set; }

        public bool Active { get; set; }

        public int SortOrder { get; set; }

        public int MaxAgeOffset { get; set; }

        public int MinAgeOffset { get; set; }
    }
}
