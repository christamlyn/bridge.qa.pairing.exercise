namespace Bridge.Academies.Api.Translators
{
    using System;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class GradeToAcademyDailyMetricsTranslator : ITranslator<Models.GradeDailyMetric, ViewModels.AcademyDailyMetrics>
    {
        public void Translate(Models.GradeDailyMetric source, ViewModels.AcademyDailyMetrics destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.ActiveCount = source.ActiveCount ?? 0;
            destination.ActivePTR = source.ActivePTR ?? 0;
            destination.AicCount = source.AicCount ?? 0;
            destination.AicPTR = Convert.ToInt32(source.AicPTR);
            if ((source.Date != null) && (source.Date.DateValue != DateTime.MinValue))
            {
                destination.Date = source.Date.DateValue;
            }

            destination.NaicCount = Convert.ToInt32(source.NaicCount);
            destination.LastUpdatedDate = source.UpdateDate;
        }
    }
}
