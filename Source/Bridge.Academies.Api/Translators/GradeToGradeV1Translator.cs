﻿namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class GradeToGradeV1Translator : ITranslator<Models.Grades, GradeV1>
    {
        public void Translate(Models.Grades source, GradeV1 destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.GradeId = source.GradeId;
            destination.GradeName = source.GradeName;
            destination.GradeAbbreviation = source.GradeAbbreviation;
            destination.HasPlacement = source.HasPlacement;
            destination.MaxAgeOffset = source.MaxAgeOffset ?? 0;
            destination.MaxPlacementScore = source.MaxPlacementScore;
            destination.MaxPupils = source.MaxPupils;
            destination.MinAgeOffset = source.MinAgeOffset ?? 0;
            destination.MinPlacementScore = source.MinPlacementScore;
            destination.SortOrder = source.SortOrder ?? 0;
            destination.Active = source.Active;
        }
    }
}
