﻿namespace Bridge.Academies.Api.Translators
{
    using System;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class ProbableNaicsToAcademyProbableNaicsTranslator : ITranslator<Models.ProbableNaics, ViewModels.AcademyProbableNaics>
    {
        public void Translate(Models.ProbableNaics source, ViewModels.AcademyProbableNaics destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();
            destination.PupilId = source.PupilId;
        }
    }
}
