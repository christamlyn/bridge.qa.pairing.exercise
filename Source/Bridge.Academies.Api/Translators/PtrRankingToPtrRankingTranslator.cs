﻿namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class PtrRankingToPtrRankingTranslator : ITranslator<Models.PtrRanking, PtrRanking>
    {
        public void Translate(Models.PtrRanking source, PtrRanking destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.AcademyId = source.AcademyId;
            destination.Ranking = source.Ranking;
            destination.TotalAcademies = source.TotalAcademies;
            destination.AicPTR = source.AicPTR.HasValue ? source.AicPTR.Value : 0;
        }
    }
}
