﻿namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class GradeToGradeTranslator : ITranslator<Models.Grades, Grade>
    {
        public void Translate(Models.Grades source, Grade destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.Code = source.GradeAbbreviation;
            destination.GradeId = source.GradeId;
            destination.Name = source.GradeName;
            destination.Order = source.SortOrder ?? 0;
            destination.MaxAgeOffset = source.MaxAgeOffset ?? 0;
            destination.MinAgeOffset = source.MinAgeOffset ?? 0;
        }
    }
}
