namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class AcademyToAcademyAllTranslator : ITranslator<Models.Academy, ViewModels.AcademyAll>
    {
        private readonly ITranslator<Models.AcademyLocation, Coordinates> academyLocationToCoordinatesTranslator;
        private readonly ITranslator<Models.Region, Location> regionToLocationTranslator;

        public AcademyToAcademyAllTranslator(
           ITranslator<Models.AcademyLocation, Coordinates> academyLocationToCoordinatesTranslator,
           ITranslator<Models.Region, Location> regionToLocationTranslator)
        {
            this.academyLocationToCoordinatesTranslator = academyLocationToCoordinatesTranslator;
            this.regionToLocationTranslator = regionToLocationTranslator;
        }

        public void Translate(Models.Academy source, AcademyAll destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.AcademyId = source.AcademyID;

            destination.AcademyManager = source.AcademyManager;

            destination.ShortCode = source.ShortCode;

            destination.Code = source.AcademyCode;

            destination.Name = source.AcademyName;

            destination.Phone = source.AcademyManagerContactPhone;

            destination.Active = source.Active;

            destination.LaunchDate = source.LaunchDate;

            destination.RegionId = source.RegionID;

            if (source.Region != null)
            {
                destination.Location = this.regionToLocationTranslator.Translate(source.Region);

                if (source.AcademyLocation != null)
                {
                    destination.Location.Coordinates = this.academyLocationToCoordinatesTranslator.Translate(source.AcademyLocation);
                }
            }
        }
    }
}
