﻿namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class AcademyToAcademyFullTranslator : AcademyToAcademyTranslator, ITranslator<Models.Academy, AcademyFull>
    {
        private readonly ITranslator<Models.AcademyLocation, Coordinates> academyLocationToCoordinatesTranslator;
        private readonly ITranslator<Models.Region, Location> regionToLocationTranslator;

        public AcademyToAcademyFullTranslator(
            ITranslator<Models.AcademyLocation, Coordinates> academyLocationToCoordinatesTranslator,
            ITranslator<Models.Region, Location> regionToLocationTranslator)
        {
            this.academyLocationToCoordinatesTranslator = academyLocationToCoordinatesTranslator;
            this.regionToLocationTranslator = regionToLocationTranslator;
        }

        public void Translate(Models.Academy source, AcademyFull destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            this.Translate(source, (Academy)destination);

            if (source.Region != null)
            {
                destination.Location = this.regionToLocationTranslator.Translate(source.Region);

                if (source.AcademyLocation != null)
                {
                    destination.Location.Coordinates = this.academyLocationToCoordinatesTranslator.Translate(source.AcademyLocation);
                }
            }
        }
    }
}
