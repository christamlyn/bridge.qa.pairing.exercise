﻿namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class AcademyToAcademyLocationTranslator : ITranslator<Models.Academy, AcademyLocation>
    {
        private readonly ITranslator<Models.AcademyLocation, Coordinates> academyLocationToCoordinatesTranslator;
        private readonly ITranslator<Models.Region, Location> regionToLocationTranslator;

        public AcademyToAcademyLocationTranslator(
            ITranslator<Models.AcademyLocation, Coordinates> academyLocationToCoordinatesTranslator,
            ITranslator<Models.Region, Location> regionToLocationTranslator)
        {
            this.academyLocationToCoordinatesTranslator = academyLocationToCoordinatesTranslator;
            this.regionToLocationTranslator = regionToLocationTranslator;
        }

        public void Translate(Models.Academy source, AcademyLocation destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.AcademyId = source.AcademyID;
            if (source.AcademyLocation != null)
            {
                destination.Coordinates = this.academyLocationToCoordinatesTranslator.Translate(source.AcademyLocation);
            }

            if (source.Region != null)
            {
                this.regionToLocationTranslator.Translate(source.Region, destination);
            }
        }
    }
}
