﻿namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class RegionToLocationTranslator : ITranslator<Models.Region, Location>
    {
        public void Translate(Models.Region source, Location destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.Country = source.Country;
            destination.Level1 = source.LevelOne;
            destination.Level2 = source.LevelTwo;
            destination.Level3 = source.LevelThree;
            destination.Level4 = source.LevelFour;
            destination.Level5 = source.LevelFive;
            destination.Level6 = source.LevelSix;
            destination.Level7 = source.LevelSeven;
        }
    }
}
