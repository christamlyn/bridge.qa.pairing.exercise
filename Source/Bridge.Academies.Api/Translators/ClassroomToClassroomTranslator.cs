namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class ClassroomToClassroomTranslator : ITranslator<Models.Classroom, Classroom>
    {
        public void Translate(Models.Classroom source, Classroom destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.Active = source.Active;
            destination.ClassroomName = source.ClassroomName;
            destination.DateInactivated = source.DateInactivated;
            destination.EffectiveDate = source.EffectiveDate;
        }
    }
}
