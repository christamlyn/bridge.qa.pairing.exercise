namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class AcademyToAcademyTranslator : ITranslator<Models.Academy, Academy>
    {
        public void Translate(Models.Academy source, Academy destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.AcademyId = source.AcademyID;
            destination.Active = source.Active;
            destination.Code = source.AcademyCode;
            destination.LaunchDate = source.LaunchDate;
            destination.Name = source.AcademyName;
            destination.Phone = source.AcademyManagerContactPhone;
            destination.ShortCode = source.ShortCode;
            destination.RegionId = source.RegionID;
        }
    }
}
