﻿namespace Bridge.Academies.Api.Translators
{
    using Bridge.Academies.Api.ViewModels;
    using Bridge.Framework;
    using Bridge.Framework.ComponentModel;

    public class AcademyLocationToCoordinatesTranslator : ITranslator<Models.AcademyLocation, Coordinates>
    {
        public void Translate(Models.AcademyLocation source, Coordinates destination)
        {
            Guard.That(source, nameof(source)).IsNotNull();
            Guard.That(destination, nameof(destination)).IsNotNull();

            destination.Latitude = source.GPSLat.GetValueOrDefault();
            destination.Longitude = source.GPSLong.GetValueOrDefault();
            destination.CommunityRadius = source.Radius.GetValueOrDefault();
        }
    }
}
