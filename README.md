# Objective

This repository aims to provide an API for Bridge academies.

# Getting Started

1. Install Visual Studio 2015 with Update 1
2. To run the unit tests, enable the following setting from the Visual Studio menu:
    > Test -> Test Settings -> Default Test Architecture -> x64

# Contributing

Before contributing any code to this repository, please ensure that it passes the following checks:

1. The code must pass [StyleCop](https://github.com/DotNetAnalyzers/StyleCopAnalyzers) code style rules as determined by the MinimumRecommendedRulesWithStyleCop.ruleset file at the root of the solution.
2. The code must pass static Code Analysis rules.
3. The code must have unit tests.
4. The code must pass a code review by a lead developer.